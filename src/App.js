import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navbar from "./components/layouts/Navbar";
import Refresh from "./components/layouts/Refresh";
import HomePage from "./components/pages/HomePage";
import Login from "./components/pages/Login";
import ProfilePage from "./components/pages/ProfilePage";
import ProfileSalesRecordsPage from "./components/pages/ProfileSalesRecordsPage";
import ProfileOrdersPage from "./components/pages/ProfileOrdersPage";
import ProfileProductsPage from "./components/pages/ProfileStorePage";
import CartPage from "./components/pages/CartPage";
import SignUp from "./components/pages/SignUp";
import ProductsPage from "./components/pages/ProductsPage";
import ProductPage from "./components/pages/ProductPage";
import SearchResultPage from "./components/pages/SearchResultPage";
import NotFound404Page from "./components/pages/NotFound404Page";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import store, { persistor } from "./store";

const listStyle = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
  height: window.innerHeight,
  minWidth: "900px",
};
class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}
  render() {
    return (
      <div style={listStyle}>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <Router>
              <Navbar />
              <div style={{ height: 70 }}>.</div>
              <Switch>
                <Route exact path="/" component={HomePage} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/profile/:type" component={ProfilePage} />
                <Route
                  exact
                  path="/profile/:type/orders"
                  component={ProfileOrdersPage}
                />
                <Route
                  exact
                  path="/profile/:type/sales-records"
                  component={ProfileSalesRecordsPage}
                />
                <Route
                  exact
                  path="/profile/:type/store"
                  component={ProfileProductsPage}
                />
                <Route exact path="/cart" component={CartPage} />
                <Route exact path="/signup/:user" component={SignUp} />
                <Route
                  exact
                  path="/products/:category/:page"
                  component={ProductsPage}
                />
                <Route
                  exact
                  path="/product/:id/:title"
                  component={ProductPage}
                />
                <Route
                  exact
                  path="/search/:text/:page"
                  component={SearchResultPage}
                />
                <Route exact path="/s" component={Refresh} />
                <Route component={NotFound404Page} />
              </Switch>
            </Router>
          </PersistGate>
        </Provider>
      </div>
    );
  }
}

export default App;

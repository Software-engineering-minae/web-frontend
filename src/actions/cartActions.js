import {
  ADD_ORDER,
  DELETE_ORDER,
  ADD_ORDER2,
  EDIT_ORDER,
  CLEAR_CART,
} from "./types";
import axios from "axios";
const baseURL = "https://hampa2.pythonanywhere.com";
export const addOrder = (id) => async (dispatch) => {
  try {
    const res = await axios.put(
      baseURL + "/carts/1/add_to_cart/",
      JSON.stringify({
        product_id: id.id,
        count: 1,
      }),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: ADD_ORDER,
      payload: id,
    });
  } catch (error) {
    dispatch({
      type: ADD_ORDER,
      payload: id,
    });
  }
};

export const deleteOrder = (id) => async (dispatch) => {
  try {
    const res = await axios.put(
      baseURL + "/carts/1/remove_from_cart/",
      JSON.stringify({
        product_id: id,
      }),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: DELETE_ORDER,
      payload: id,
    });
  } catch (error) {
    dispatch({
      type: DELETE_ORDER,
      payload: id,
    });
  }
};
export const refresh = (id) => async (dispatch) => {
  const res = await axios.get(baseURL + "/carts/retrieve_cart/");
  dispatch({
    type: ADD_ORDER2,
    payload: res.data.cart_items,
  });
};

export const clearCart = () => async (dispatch) => {
  dispatch({
    type: CLEAR_CART,
    payload: {},
  });
};

export const editOrder = (order) => async (dispatch) => {
  dispatch({
    type: EDIT_ORDER,
    payload: order,
  });
};

export const addProductToCart = (id, value) => async (dispatch) => {
  const res = await axios.put(
    baseURL + "/carts/1/add_to_cart/",
    JSON.stringify({
      product_id: id,
      count: value,
    }),
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
};

export const submitOrder = (total, id) => async (dispatch) => {
  const res = await axios.post(
    baseURL + "/orders/",
    JSON.stringify({
      total: total,
      address_id: id,
    }),
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  return res;
};

export const retrieveCart = () => async (dispatch) => {
  const res = await axios.get(baseURL + "/carts/retrieve_cart/");
  return res;
};

export const getOrders = () => async (dispatch) => {
  const res = await axios.get(baseURL + "/orders/order_history/");
  return res;
};

export const getSalesRecords = () => async (dispatch) => {
  const res = await axios.get(baseURL + "/orders/retrieve_sales_record/");
  return res;
};

export const changeStatus = (items, status) => async (dispatch) => {
  const res = await axios.put(
    baseURL + "/orders/1/change_order_status/",
    JSON.stringify({
      order_id_list: items,
      status: status,
    }),
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  return res;
};

import axios from "axios";
const baseURL = "https://hampa2.pythonanywhere.com";

export const getCategories = () => async (dispatch) => {
  const res = await axios.get(baseURL + "/products/category/");
  return res;
};

export const getAllProducts = () => async (dispatch) => {
  const res = await axios.get(baseURL + "/products/");
  return res;
};

export const getNews = () => async (dispatch) => {
  const res = await axios.get(baseURL + "/products/static-url/");
  return res;
};

export const likeComment = (id, like) => async (dispatch) => {
  await axios.post(baseURL + "/products/add-likes/", {
    commentID: id,
    like: like,
  });
};

export const getComments = (id) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/comments/", {
    productID: id,
  });
  return res;
};

export const addComment = (id, text) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/add-comment/", {
    productID: id,
    text: text,
  });
};

export const getProductDetail = (id) => async (dispatch) => {
  const producrRes = await axios.post(baseURL + "/products/product-detail/", {
    productID: id,
  });
  return producrRes;
};

export const checkRate = (id) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/check-rate/", {
    productID: id,
  });
  return res;
};

export const addRate = (id, value) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/add-rate/", {
    productID: id,
    rate: value,
  });
};

export const getRate = (id) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/rate/", { productID: id });
  return res;
};

export const getCategory = (category) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/category/", {
    choice: category,
  });
  return res;
};

export const deleteProduct = (id) => async (dispatch) => {
  const res = await axios.post(baseURL + "/producers/delete-product/", {
    productID: id,
  });
};

export const getUserProducts = () => async (dispatch) => {
  const res = await axios.get(baseURL + "/producers/user-products/");
  return res;
};

export const addProduct = (bodyFormData) => async (dispatch) => {
  const res = await axios.post(
    baseURL + "/producers/add-product/",
    bodyFormData,
    { headers: { "Content-Type": "multipart/form-data" } }
  );
};

export const editProduct = (bodyFormData) => async (dispatch) => {
  const res = await axios.put(
    baseURL + "/producers/update-product/",
    bodyFormData,
    { headers: { "Content-Type": "multipart/form-data" } }
  );
};

export const search = (text) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/search/", { input: text });
  return res;
};

export const getPopularProducts = () => async (dispatch) => {
  const res = await axios.get(baseURL + "/orders/retrieve_popular_products/");
  return res;
};

export const getSimilarProducts = (id) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/similar-products/", {
    productID: id,
  });
  return res;
};

export const getCategoryPage = (category, page) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/category/", {
    choice: category,
    page: page,
  });
  return res;
};

export const getCategoryTotalPages = (category) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/total/", {
    cat: category,
  });
  return res;
};

export const searchPage = (text, page) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/search/", {
    input: text,
    page: page,
  });
  return res;
};

export const searchTotalPages = (text) => async (dispatch) => {
  const res = await axios.post(baseURL + "/products/total-search/", {
    input: text,
  });
  return res;
};

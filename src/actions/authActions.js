import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import * as EmailValidator from "email-validator";
import decode from "jwt-decode";

import {
  GET_ERRORS,
  SET_CURRENT_USER,
  USER_LOGOUT,
  DELET_ERRORS,
  SET_TOKEN,
} from "./types";
import newToken from "../utils/newToken";
const baseURL = "https://hampa2.pythonanywhere.com";

export const signupUser = (userData, history) => async (dispatch) => {
  let errors = {};
  let hasError = false;

  if (userData.first_name === "") {
    errors.first_name = "This field is required";
    hasError = true;
  }
  if (userData.last_name === "") {
    errors.last_name = "This field is required";
    hasError = true;
  }
  if (userData.username === "") {
    errors.username = "This field is required";
    hasError = true;
  } else if (userData.username.length < 6) {
    errors.username = "Your username should be at least 6 character";
    hasError = true;
  }
  if (userData.email === "") {
    errors.email = "This field is required";
    hasError = true;
  } else if (!EmailValidator.validate(userData.email)) {
    errors.email = "This email not valid";
    hasError = true;
  }
  if (userData.phone_number.length !== 11) {
    errors.phone_number = "phone number should be 11 digit";
    hasError = true;
  }
  if ((parseInt(userData.phone_number) + "").length !== 10) {
    errors.phone_number = "wrong phone number ";
    hasError = true;
  }
  if (userData.phone_number === "") {
    errors.phone_number = "This field is required";
    hasError = true;
  }

  if (userData.password === "") {
    errors.password = "This field is required";
    hasError = true;
  } else if (userData.password.length < 6) {
    errors.password = "Your password should be at least 6 character";
    hasError = true;
  }
  if (userData.password2 === "") {
    errors.password2 = "This field is required";
    hasError = true;
  }
  if (userData.password !== userData.password2) {
    errors.password2 = "Not matched";
    hasError = true;
  }
  if (hasError) {
    dispatch({ type: GET_ERRORS, payload: errors });
    setTimeout(() => {
      dispatch({ type: DELET_ERRORS, payload: {} });
    }, 4000);
    return;
  }
  await axios
    .post(baseURL + "/accounts/signup/", userData)
    .then((res) => history.push("/login"))
    .catch((error) => {
      dispatch({
        type: GET_ERRORS,
        payload: error.response.data,
      });
      setTimeout(() => {
        dispatch({ type: DELET_ERRORS, payload: {} });
      }, 4000);
    });
};

export const loginUser = (userData) => async (dispatch) => {
  let errors = {};
  let hasError = false;

  if (userData.username === "") {
    errors.username = "This field is required";
    hasError = true;
  } else if (userData.username.length < 6) {
    errors.username = "Your username should be at least 6 character";
    hasError = true;
  }
  if (userData.password === "") {
    errors.password = "This field is required";
    hasError = true;
  }
  if (hasError) {
    dispatch({ type: GET_ERRORS, payload: errors });
    setTimeout(() => {
      dispatch({ type: DELET_ERRORS, payload: {} });
    }, 4000);
    return;
  }
  await axios
    .post(baseURL + "/accounts/login/", userData)
    .then((res) => {
      const { access, refresh } = res.data;
      localStorage.setItem("jwtRefToken", refresh);
      setAuthToken(access);
      getUserInfo(dispatch);
      dispatch({
        type: SET_TOKEN,
        payload: { token: access, refreshToken: refresh },
      });
    })
    .catch((error) => {
      console.log(error);

      dispatch({
        type: GET_ERRORS,
        payload: { UserError: error },
      });
      setTimeout(() => {
        dispatch({ type: DELET_ERRORS, payload: {} });
      }, 4000);
    });
};

export const setCurrentUser = (user) => {
  return {
    type: SET_CURRENT_USER,
    payload: user,
  };
};

export const logoutUser = () => (dispatch) => {
  localStorage.removeItem("jwtRefToken");
  setAuthToken(false);
  dispatch({
    type: USER_LOGOUT,
    payload: {},
  });
};

export const getUserInfo = async (dispatch) => {
  try {
    const { data } = await axios.get(baseURL + "/accounts/retrieve/");

    dispatch({
      type: "SET_CURRENT_USER",
      payload: data,
    });
  } catch (error) {
    dispatch({ type: "GET_ERRORS", payload: error });
  }
};

export const checkAuth = () => async (dispatch) => {
  const refreshToken = localStorage.getItem("jwtRefToken");
  if (!refreshToken) {
    return true;
  }
  try {
    const { exp } = decode(refreshToken);
    if (exp * 1000 < new Date().getTime()) {
      return true;
    }
  } catch (e) {
    return true;
  }
  const accessToken = await newToken();
  setAuthToken(accessToken);
  await getUserInfo(dispatch);
  return true;
};

export const getAccountInfo = () => async (dispatch) => {
  const res = await axios.get(baseURL + "/accounts/retrieve/");
  return res;
};

export const getProfileInfo = () => async (dispatch) => {
  const res = await axios.get(baseURL + "/profile/retrieve_profile/");
  return res;
};

export const addNewAddress = (province, city, address) => async (dispatch) => {
  const res = await axios.put(
    baseURL + "/address/1/add_address/",
    JSON.stringify({
      province: province,
      city: city,
      address: address,
    }),
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
};

export const deleteAddress = (id) => async (dispatch) => {
  const res = await axios.delete(baseURL + "/address/1/remove_address/", {
    data: { address_id: id },
  });
};

export const editProfile = (bodyFormData) => async (dispatch) => {
  try {
    const res = await axios
      .put(baseURL + "/profile/1/update_profile/", bodyFormData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then(async (res) => {
        await getUserInfo(dispatch);
      });
    return true;
  } catch {
    return false;
  }
};

export const addCredit = (credit) => async (dispatch) => {
  try {
    const res = await axios.put(baseURL + "/profile/1/add_credit/", {
      additional_credit: credit,
    });
    return true;
  } catch {
    return false;
  }
};

export const checkToken = () => async (dispatch) => {
  try {
    const res = await axios.get(baseURL + "/accounts/retrieve/");
    return true;
  } catch {
    return false;
  }
};

export const setToken = (access, refresh) => {
  axios.defaults.headers = {
    Authorization: "Bearer " + access,
  };
  return {
    type: SET_TOKEN,
    payload: { token: access, refreshToken: refresh },
  };
};

export const sendTicket = (subject, context) => async (dispatch) => {
  try {
    const res = await axios.post(baseURL + "/accounts/ticket/", {
      subject: subject,
      context: context,
    });
    return true;
  } catch {
    return false;
  }
};

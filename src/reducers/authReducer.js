import {
  SET_CURRENT_USER,
  GET_USER_INFO,
  USER_LOGOUT,
  SET_TOKEN,
} from "../actions/types";
// import isEmpty from "../validations/is-empty";

const initialState = {
  isAuthenticated: false,
  user: {},
  token: "",
  refreshToken: "",
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload,
      };
    case GET_USER_INFO:
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload,
      };
    case USER_LOGOUT:
      return {
        isAuthenticated: false,
        user: {},
      };
    case SET_TOKEN:
      return {
        ...state,
        token: action.payload.token,
        refreshToken: action.payload.refreshToken,
      };
    default:
      return state;
  }
};

export default authReducer;

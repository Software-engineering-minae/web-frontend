import { DELETE_ORDER, ADD_ORDER,ADD_ORDER2 , EDIT_ORDER , CLEAR_CART} from "../actions/types";
const initialState = {
    cart : [],
    cart2:[]
  };
  
  const designReducer = (state = initialState, action) => {
    switch (action.type) {
      case ADD_ORDER:
        
        return {
          ...state,
          cart: [...state.cart,{product : action.payload  , id:state.cart.length ,quantity : 1 , isOpen:false}]
        };
      case ADD_ORDER2:
      
          return {
            ...state,
            cart: action.payload
          };
      case DELETE_ORDER:
        
        return {
         
          ...state,
          cart: state.cart.filter(
            order => order.product.id+"" !== action.payload+""
          )
        };
        case EDIT_ORDER:
        
            return {
             
              ...state,
              cart: state.cart.map(
                order => order.product.id+"" === action.payload.product.id+""
                  ?(order=action.payload)
                  :order
              )
            };
      case CLEAR_CART:
  
        return {
          
          ...state,
          cart:[] 
          
        };
      default:
        return state;
    }
  };
  
  export default designReducer;
  
import axios from "axios";

const setAuthToken = accToken => {
  if (accToken) {
    axios.defaults.headers = { Authorization: "Bearer " + accToken };
  } else {
    delete axios.defaults.headers;
  }
};

export default setAuthToken;

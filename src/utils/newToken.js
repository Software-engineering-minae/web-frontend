import axios from "axios";

const newToken = async () => {
  const refToken = localStorage.getItem("jwtRefToken");
  console.log("in token");
  if (refToken) {
    try {
      const res = await axios.post(
        "https://hampa2.pythonanywhere.com/accounts/login/refresh/",
        { refresh: refToken }
      );
      return res.data.access;
    } catch (e) {
      return null;
    }
  }
  return null;
};

export default newToken;

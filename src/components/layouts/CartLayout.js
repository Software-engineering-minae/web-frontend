import React, { Component } from 'react';
import { connect } from "react-redux";
import axios from "axios";
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import {  refresh , retrieveCart} from "../../actions/cartActions";




class CartLayout extends Component {
    constructor(props) {
        super(props);
        this.state={
          count : 0,
          isLoggedIn:false
        };
      }
      
    goToCart(){
        this.props.history.push("/cart");
    }
    async componentDidMount(){
      if (!this.props.auth.isAuthenticated) {
        await this.setState({isLoggedIn: false});
      } 
      else {
        this.props.refresh()
        await this.setState({isLoggedIn: true});
      }
      if(this.state.isLoggedIn==true)
      {
        const res = await this.props.retrieveCart();
        await this.setState({ count: Object.keys(res.data.cart_items).length });
      }
      else{
        await this.setState({ count: Object.keys(this.props.cart.cart).length });
      }
    }
    async componentWillReceiveProps(nextProps){
      if(this.state.isLoggedIn==true)
      {
        const res = await this.props.retrieveCart();
        await this.setState({ count: Object.keys(res.data.cart_items).length });
      }
      else{
        await this.setState({count:Object.keys(nextProps.cart.cart).length})
      }

    }
      
    render() {
        return (
            <div style={{borderRadius:"50%"   ,height:"100px", position:"fixed" , bottom : "100px" , right :"40px"}}>
                <p  style={{color:"#d7385e" , position:"absolute" , top:"0px" , left:"-26px" , fontSize:"25px" , fontStyle:"bold"}}>{this.state.count}</p>
            <ShoppingCartOutlinedIcon
                style={{color:"#d7385e" , cursor:"pointer" ,position:"absolute",top:"45px" ,left:"-50px" , width:"70px" , height:"70px"}}
                 onClick={this.goToCart.bind(this)}
                ></ShoppingCartOutlinedIcon>
            </div>
        )
    }
}

  CartLayout.propTypes = {
    cart: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    refresh: PropTypes.func.isRequired,
    retrieveCart :PropTypes.func.isRequired,




  };
  
  const mapStateToProps = state => ({
    cart: state.cart,
    cart2: state.cart2,
    auth: state.auth,


  });
  
  export default withRouter(connect(mapStateToProps, {refresh,retrieveCart })(CartLayout));
  
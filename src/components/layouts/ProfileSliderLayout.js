import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import Slide from "@material-ui/core/Slide";
import MailIcon from "@material-ui/icons/Mail";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import StoreIcon from "@material-ui/icons/Store";
import { Link, Dialog, IconButton } from "@material-ui/core";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Hidden from "@material-ui/core/Hidden";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { withRouter } from "react-router-dom";
import axios from "axios";
import Avatar from "@material-ui/core/Avatar";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import FaceIcon from "@material-ui/icons/Face";
import EditIcon from "@material-ui/icons/Edit";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import HelpIcon from "@material-ui/icons/Help";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import TimelineIcon from "@material-ui/icons/Timeline";
import { logoutUser, addCredit } from "../../actions/authActions";
import Proptypes from "prop-types";
import { clearCart } from "../../actions/cartActions";
import {
  getAccountInfo,
  getProfileInfo,
  editProfile,
  sendTicket,
} from "../../actions/authActions";
import { exportDefaultSpecifier } from "@babel/types";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import PropType from "prop-types";
import Badge from "@material-ui/core/Badge";
import AddAPhotoIcon from "@material-ui/icons/AddAPhoto";
import * as EmailValidator from "email-validator";
import "fontsource-roboto";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import AddIcon from "@material-ui/icons/Add";

const bgColor = "#011627";
const imageURL =
  "https://www.pythonanywhere.com/user/hampa2/files/home/hampa2/";

function SlideTransition(props) {
  return <Slide {...props} direction="up" />;
}

class ProfileSliderLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      userProfile: "",
      dialogIsOpen: false,
      first_name: "",
      last_name: "",
      username: "",
      email: "",
      phone_number: "",
      password: "",
      password2: "",
      avatar: "",
      hasImage: false,
      image: "",
      formData: [],
      firstnameError: { error: false, text: "" },
      lastnameError: { error: false, text: "" },
      usernameError: { error: false, text: "" },
      emailError: { error: false, text: "" },
      phonenumberError: { error: false, text: "" },
      passwordError: { error: false, text: "" },
      subject: "",
      text: "",
      subjectError: { error: false, text: "" },
      textError: { error: false, text: "" },
      supportDialogIsOpen: false,
      snackbarIsOpen: false,
      snackbar2IsOpen: false,
      addCreditDialogIsOpen: false,
      creditError: { error: false, text: "" },
      credit: 0,
      snackbarMessage: "",
      snackber2Message: "please try again",
    };
  }
  addCreditClicked() {
    this.setState({ addCreditDialogIsOpen: true });
  }
  onClickAddCredit = async (e) => {
    e.preventDefault();
    if (
      (parseInt(this.state.credit) + "").length != this.state.credit.length ||
      parseInt(this.state.credit) <= 0
    ) {
      this.setState({
        creditError: { error: true, text: "should be a positive number" },
      });
    } else {
      this.setState({
        creditError: { error: false, text: "should be a positive number" },
      });
      const res = await this.props.addCredit(this.state.credit);
      if (res == true) {
        const res2 = await this.props.getProfileInfo();
        await this.setState({
          userProfile: res2.data,
          snackbarIsOpen: true,
          addCreditDialogIsOpen: false,
          snackbarMessage: "credit successfully added",
        });
      } else {
        this.setState({
          snackbar2IsOpen: true,
          addCreditDialogIsOpen: false,
          snackber2Message: "please try again",
        });
      }
    }
  };
  onClickCancelCredit() {
    this.setState({ addCreditDialogIsOpen: false, credit: 0 });
  }
  handleCloseSnackbar() {
    this.setState({ snackbarIsOpen: false, snackbar2IsOpen: false });
  }
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onClickCancelEdit() {
    this.setState({ dialogIsOpen: false });
  }
  onClickCancelTicket() {
    this.setState({ supportDialogIsOpen: false });
  }
  onClickSend = async (e) => {
    e.preventDefault();

    let hasError = false;
    if (this.state.subject === "") {
      this.setState({
        subjectError: { error: true, text: "cant be empty" },
      });
      hasError = true;
    } else {
      this.setState({
        subjectError: { error: false, text: "cant be empty" },
      });
    }
    if (this.state.text === "") {
      this.setState({
        textError: { error: true, text: "cant be empty" },
      });

      hasError = true;
    } else {
      this.setState({
        textError: { error: false, text: "cant be empty" },
      });
    }
    if (hasError === false) {
      const res = await this.props.sendTicket(
        this.state.subject,
        this.state.text
      );
      if (res === true) {
        this.setState({
          snackbarIsOpen: true,
          snackbarMessage: "successfully sent",
          supportDialogIsOpen: false,
        });
      } else {
        this.setState({
          snackbar2IsOpen: true,
          snackbar2Message: "please try again",
          supportDialogIsOpen: false,
        });
      }
    }
  };
  onClickSave = async (e) => {
    e.preventDefault();
    let hasError = false;

    if (this.state.first_name === "") {
      this.setState({
        firstnameError: { error: true, text: "This field is required" },
      });
      hasError = true;
    } else {
      this.setState({ firstnameError: { error: false, text: "" } });
    }
    if (this.state.last_name === "") {
      this.setState({
        lastnameError: { error: true, text: "This field is required" },
      });
      hasError = true;
    } else {
      this.setState({ lastnameError: { error: false, text: "" } });
    }
    if (this.state.username === "") {
      this.setState({
        usernameError: { error: true, text: "This field is required" },
      });
      hasError = true;
    } else if (this.state.username.length < 6) {
      this.setState({
        usernameError: {
          error: true,
          text: "Your username should be at least 6 character",
        },
      });
      hasError = true;
    } else {
      this.setState({ usernameError: { error: false, text: "" } });
    }
    if (this.state.email === "") {
      this.setState({
        emailError: { error: true, text: "This field is required" },
      });
      hasError = true;
    } else if (!EmailValidator.validate(this.state.email)) {
      this.setState({
        emailError: { error: true, text: "This email not valid" },
      });
      hasError = true;
    } else {
      this.setState({ emailError: { error: false, text: "" } });
    }

    if (this.state.phone_number === "") {
      this.setState({
        phonenumberError: { error: true, text: "This field is required" },
      });
      hasError = true;
    } else if (this.state.phone_number.length !== 11) {
      this.setState({
        phonenumberError: {
          error: true,
          text: "phone number should be 11 digit",
        },
      });
      hasError = true;
    } else if ((parseInt(this.state.phone_number) + "").length !== 10) {
      this.setState({
        phonenumberError: { error: true, text: "wrong phone number " },
      });
      hasError = true;
    } else {
      this.setState({ phonenumberError: { error: false, text: "" } });
    }

    if (this.state.password.length > 0 || this.state.password2 > 0) {
      if (this.state.password.length < 6) {
        this.setState({
          passwordError: {
            error: true,
            text: "Your password should be at least 6 character",
          },
        });
        hasError = true;
      } else if (this.state.password !== this.state.password2) {
        this.setState({ passwordError: { error: true, text: "Not matched" } });
        hasError = true;
      } else {
        this.setState({ passwordError: { error: false, text: "" } });
      }
    } else {
      this.setState({ passwordError: { error: false, text: "" } });
    }

    if (hasError == false) {
      if (
        this.state.image == imageURL + this.state.userProfile.image ||
        this.state.hasImage == false
      ) {
        var bodyFormData = new FormData();
      } else {
        var bodyFormData = this.state.formData;
      }
      bodyFormData.set("first_name", this.state.first_name);
      bodyFormData.set("last_name", this.state.last_name);
      bodyFormData.set("username", this.state.username);
      bodyFormData.set("email", this.state.email);
      bodyFormData.set("phone_number", this.state.phone_number);
      if (this.state.password.length > 0) {
        bodyFormData.set("password", this.state.password);
      }

      const res = await this.props.editProfile(bodyFormData);
      if (res == true) {
        this.props.history.push({
          pathname: "/s/",
          state: { type: "profile", param: this.props.type },
        });
      } else {
        this.setState({
          usernameError: { error: true, text: "username already taken" },
        });
        hasError = true;
      }
    }
  };
  onImageChange = async (event) => {
    let image = event.target.files;

    if (image && image[0]) {
      let fd = new FormData();
      fd.append("image", image[0]);
      await this.setState({
        formData: fd,
        image: URL.createObjectURL(image[0]),
        hasImage: true,
      });
    }
  };

  goToCart() {
    this.props.history.push("/cart");
  }
  async logout() {
    await this.props.logoutUser();
    await this.props.clearCart();
    this.props.history.push({
      pathname: "/s",
      state: { type: "logout" },
    });
  }

  async drawerOnClick(url) {
    if (url == "/profile/" + this.props.type + "/store") {
      this.props.history.push({ pathname: url });
    } else if (url == "/profile/" + this.props.type + "/sales-records") {
      this.props.history.push({ pathname: url });
    } else if (url == "/support") {
      this.setState({ subject: "", text: "", supportDialogIsOpen: true });
    } else if (url == "/editprofile") {
      this.setState({
        last_name: this.state.user.last_name,
        first_name: this.state.user.first_name,
        username: this.state.user.username,
        email: this.state.user.email,
        phone_number: this.state.user.phone_number,
        dialogIsOpen: true,
        formData: "",
        firstnameError: { error: false, text: "" },
        lastnameError: { error: false, text: "" },
        usernameError: { error: false, text: "" },
        emailError: { error: false, text: "" },
        phonenumberError: { error: false, text: "" },
        passwordError: { error: false, text: "" },
      });
      if (this.state.userProfile.image != null) {
        await this.setState({
          hasImage: true,
          image: imageURL + this.state.userProfile.image,
        });
      } else {
        await this.setState({ hasImage: false, image: "" });
      }
    } else {
      this.props.history.push({ pathname: url });
    }
  }
  async componentDidMount() {
    const res = await this.props.getAccountInfo();
    const res2 = await this.props.getProfileInfo();
    await this.setState({ user: res.data, userProfile: res2.data });
    if (this.state.userProfile.image != null) {
      await this.setState({
        hasImage: true,
        image: imageURL + this.state.userProfile.image,
      });
    } else {
      await this.setState({ hasImage: false, image: "" });
    }
  }
  render() {
    const producersItems = (
      <ListItem
        style={{ marginTop: "15px" }}
        button
        onClick={() =>
          this.drawerOnClick("/profile/" + this.props.type + "/store")
        }
      >
        <ListItemIcon>
          <StoreIcon
            style={{
              width: "40px",
              height: "40px",
              color: bgColor,
              marginLeft: "10px",
            }}
          />
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography type="body2" style={{ fontSize: "20px" }}>
              store
            </Typography>
          }
        ></ListItemText>
      </ListItem>
    );
    const producersItems2 = (
      <ListItem
        style={{ marginTop: "15px" }}
        button
        onClick={() =>
          this.drawerOnClick("/profile/" + this.props.type + "/sales-records")
        }
      >
        <ListItemIcon>
          <TimelineIcon
            style={{
              width: "40px",
              height: "40px",
              color: bgColor,
              marginLeft: "10px",
            }}
          />
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography type="body2" style={{ fontSize: "20px" }}>
              sales records
            </Typography>
          }
        ></ListItemText>
      </ListItem>
    );
    return (
      <TableCell
        style={{
          width: "300px",
          border: "none",
          display: "flex",
          justifyContent: "top",
          alignItems: "top",
        }}
      >
        <TableRow style={{ height: "50px", backgroundColor: "red" }}></TableRow>
        <TableRow
          style={{
            backgroundColor: "#f4f3f1",
            position: "static",
            top: "140px",
          }}
        >
          <List
            style={{
              width: "300px",
              backgroundColor: "#f4f3f1",
              marginLeft: "10px",
              marginRight: "10px",
            }}
          >
            <ListItem style={{ marginTop: "15px", fontSize: "10px" }}>
              <ListItemIcon>
                {!(this.state.userProfile.image === null) ? (
                  <Avatar
                    alt={this.state.user.username}
                    src={imageURL + this.state.userProfile.image}
                    style={{ width: "60px", height: "60px" }}
                  />
                ) : (
                  <FaceIcon
                    style={{ width: "60px", height: "60px", color: bgColor }}
                  />
                )}
              </ListItemIcon>
              <ListItemText>
                <TableRow>
                  {" "}
                  <Typography
                    type="body2"
                    style={{ fontSize: "20px", marginLeft: "15px" }}
                  >
                    {this.state.user.username}
                  </Typography>
                </TableRow>
                <TableRow>
                  {" "}
                  <Button
                    onClick={this.addCreditClicked.bind(this)}
                    style={{ backgroundColor: "#f4f3f1", border: "none" }}
                    endIcon={<AddIcon />}
                    variant="outlined"
                  >
                    <Typography
                      type="body2"
                      style={{
                        fontSize: "14px",
                        cursor: "pointer",
                      }}
                    >
                      credit : {this.state.userProfile.credit}
                    </Typography>{" "}
                  </Button>
                </TableRow>
              </ListItemText>
            </ListItem>

            <Divider />
            <ListItem
              style={{ marginTop: "15px", fontSize: "10px" }}
              button
              onClick={() => this.drawerOnClick("/profile/" + this.props.type)}
            >
              <ListItemIcon>
                <AccountCircleIcon
                  style={{
                    width: "40px",
                    height: "40px",
                    color: bgColor,
                    marginLeft: "10px",
                  }}
                />
              </ListItemIcon>
              <ListItemText
                primary={
                  <Typography type="body2" style={{ fontSize: "20px" }}>
                    profile
                  </Typography>
                }
              />
            </ListItem>
            <ListItem
              style={{ marginTop: "15px", fontSize: "10px" }}
              button
              onClick={() => this.drawerOnClick("/editprofile")}
            >
              <ListItemIcon>
                <EditIcon
                  style={{
                    width: "40px",
                    height: "40px",
                    color: bgColor,
                    marginLeft: "10px",
                  }}
                />
              </ListItemIcon>
              <ListItemText
                primary={
                  <Typography type="body2" style={{ fontSize: "20px" }}>
                    edit info
                  </Typography>
                }
              />
            </ListItem>

            <ListItem
              style={{ marginTop: "15px" }}
              button
              onClick={() => this.goToCart()}
            >
              <ListItemIcon>
                <ShoppingCartIcon
                  style={{
                    width: "40px",
                    height: "40px",
                    color: bgColor,
                    marginLeft: "10px",
                  }}
                />
              </ListItemIcon>
              <ListItemText
                primary={
                  <Typography type="body2" style={{ fontSize: "20px" }}>
                    cart
                  </Typography>
                }
              />
            </ListItem>
            <ListItem
              style={{ marginTop: "15px" }}
              button
              onClick={() =>
                this.drawerOnClick("/profile/" + this.props.type + "/orders")
              }
            >
              <ListItemIcon>
                <ShoppingBasketIcon
                  style={{
                    width: "40px",
                    height: "40px",
                    color: bgColor,
                    marginLeft: "10px",
                  }}
                />
              </ListItemIcon>
              <ListItemText
                primary={
                  <Typography type="body2" style={{ fontSize: "20px" }}>
                    orders history
                  </Typography>
                }
              />
            </ListItem>
          </List>
          <Divider />
          <List
            style={{
              width: "300px",
              backgroundColor: "#f4f3f1",
              marginLeft: "10px",
              marginRight: "10px",
            }}
          >
            {this.props.type + "" === 2 + "" ? producersItems : null}
            {this.props.type + "" === 2 + "" ? producersItems2 : null}
            <ListItem
              style={{ marginTop: "15px" }}
              button
              onClick={() => this.drawerOnClick("/support")}
            >
              <ListItemIcon>
                <HelpIcon
                  style={{
                    width: "40px",
                    height: "40px",
                    color: bgColor,
                    marginLeft: "10px",
                  }}
                />
              </ListItemIcon>
              <ListItemText
                primary={
                  <Typography type="body2" style={{ fontSize: "20px" }}>
                    support team
                  </Typography>
                }
              ></ListItemText>
            </ListItem>{" "}
            <ListItem
              style={{ marginTop: "15px" }}
              button
              onClick={() => this.logout()}
            >
              <ListItemIcon>
                <ExitToAppIcon
                  style={{
                    width: "40px",
                    height: "40px",
                    color: bgColor,
                    marginLeft: "10px",
                  }}
                />
              </ListItemIcon>
              <ListItemText
                primary={
                  <Typography type="body2" style={{ fontSize: "20px" }}>
                    logout
                  </Typography>
                }
              ></ListItemText>
            </ListItem>
          </List>
        </TableRow>
        <Dialog
          style={{ width: "100%", height: "100%" }}
          aria-labelledby="customized-dialog-title"
          open={this.state.dialogIsOpen}
        >
          <Container component="main" maxWidth="sm">
            <CssBaseline />
            <div>
              <Container align="center">
                <form noValidate style={{ margin: "30px" }}>
                  {!(this.state.hasImage === false) ? (
                    <Badge
                      overlap="circle"
                      anchorOrigin={{
                        vertical: "bottom",
                        horizontal: "right",
                      }}
                      badgeContent={
                        <IconButton
                          variant="contained"
                          style={{
                            width: "45px",
                            height: "45px",
                            backgroundColor: "#ffffff",
                          }}
                          component="label"
                        >
                          <AddAPhotoIcon
                            style={{
                              width: "35px",
                              height: "35px",
                              color: "#444054",
                              cursor: "pointer",
                              align: "center",
                            }}
                          />

                          <input
                            onChange={this.onImageChange}
                            type="file"
                            style={{ display: "none" }}
                          />
                        </IconButton>
                      }
                    >
                      <Avatar
                        alt={this.state.user.username}
                        src={this.state.image}
                        style={{
                          width: "200px",
                          height: "200px",
                          align: "center",
                          color: bgColor,
                          marginTop: "10px",
                        }}
                      />{" "}
                    </Badge>
                  ) : (
                    <Badge
                      overlap="circle"
                      anchorOrigin={{
                        vertical: "bottom",
                        horizontal: "right",
                      }}
                      badgeContent={
                        <IconButton
                          variant="contained"
                          style={{
                            width: "45px",
                            height: "45px",
                            backgroundColor: "#ffffff",
                          }}
                          component="label"
                        >
                          <AddAPhotoIcon
                            style={{
                              width: "35px",
                              height: "35px",
                              color: "#444054",
                              cursor: "pointer",
                              align: "center",
                            }}
                          />

                          <input
                            onChange={this.onImageChange}
                            type="file"
                            style={{ display: "none" }}
                          />
                        </IconButton>
                      }
                    >
                      <AccountCircleIcon
                        align="center"
                        style={{
                          width: "200px",
                          height: "200px",
                          align: "center",
                          color: bgColor,
                          marginTop: "10px",
                        }}
                      />
                    </Badge>
                  )}
                </form>
              </Container>
              <form
                noValidate
                onSubmit={this.onClickSave}
                style={{ marginTop: "30px" }}
              >
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      autoComplete="fname"
                      name="first_name"
                      variant="outlined"
                      required
                      fullWidth
                      id="first_name"
                      label="First Name"
                      autoFocus
                      value={this.state.first_name}
                      onChange={this.onChange}
                      error={Boolean(this.state.firstnameError.error)}
                      type="text"
                    />
                    {this.state.firstnameError.error ? (
                      <div style={{ color: "red" }}>
                        {this.state.firstnameError.text}
                        <br />{" "}
                      </div>
                    ) : null}
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      id="last_name"
                      label="Last Name"
                      name="last_name"
                      autoComplete="lname"
                      value={this.state.last_name}
                      onChange={this.onChange}
                      error={Boolean(this.state.lastnameError.error)}
                      type="text"
                    />
                    {this.state.lastnameError.error ? (
                      <div style={{ color: "red" }}>
                        {this.state.lastnameError.text}
                        <br />{" "}
                      </div>
                    ) : null}
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      id="username"
                      label="Username"
                      name="username"
                      autoFocus
                      value={this.state.username}
                      onChange={this.onChange}
                      error={Boolean(this.state.usernameError.error)}
                      type="text"
                    />
                    {this.state.usernameError.error ? (
                      <div style={{ color: "red" }}>
                        {this.state.usernameError.text}
                        <br />{" "}
                      </div>
                    ) : null}
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      id="email"
                      label="Email Address"
                      name="email"
                      autoComplete="email"
                      value={this.state.email}
                      onChange={this.onChange}
                      error={Boolean(this.state.emailError.error)}
                      type="email"
                    />
                    {this.state.emailError.error ? (
                      <div style={{ color: "red" }}>
                        {this.state.emailError.text}
                        <br />{" "}
                      </div>
                    ) : null}
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      id="phone"
                      label="Phone"
                      name="phone_number"
                      autoComplete="phone"
                      value={this.state.phone_number}
                      onChange={this.onChange}
                      error={Boolean(this.state.phonenumberError.error)}
                      type="text"
                    />
                    {this.state.phonenumberError.error ? (
                      <div style={{ color: "red" }}>
                        {this.state.phonenumberError.text}
                        <br />{" "}
                      </div>
                    ) : null}
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      fullWidth
                      name="password"
                      label="New Password"
                      type="password"
                      id="password"
                      autoComplete="current-password"
                      value={this.state.password}
                      onChange={this.onChange}
                      error={Boolean(this.state.passwordError.error)}
                    />
                    {this.state.passwordError.error ? (
                      <div style={{ color: "red" }}>
                        {this.state.passwordError.text}
                        <br />{" "}
                      </div>
                    ) : null}
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      fullWidth
                      name="password2"
                      label="Confirm New Password"
                      type="password"
                      id="password2"
                      autoComplete="current-password"
                      value={this.state.password2}
                      onChange={this.onChange}
                      error={Boolean(this.state.passwordError.error)}
                      type="password"
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={2}
                  style={{ width: "100%", margin: "15px" }}
                >
                  <Grid item xs={12} sm={6}>
                    <Button
                      style={{ backgroundColor: "#011627", padding: 10 }}
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                    >
                      save
                    </Button>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Button
                      fullWidth
                      style={{ backgroundColor: "#011627", padding: 10 }}
                      variant="contained"
                      color="primary"
                      onClick={this.onClickCancelEdit.bind(this)}
                    >
                      cancel
                    </Button>
                  </Grid>
                </Grid>
              </form>
              <div>
                {this.props.errors.UserError ? (
                  <div style={{ color: "red" }}>
                    This username already exist
                    <br />{" "}
                  </div>
                ) : null}
              </div>
            </div>
          </Container>
        </Dialog>
        <Dialog
          style={{ width: "100%", height: "100%" }}
          aria-labelledby="customized-dialog-title"
          open={this.state.supportDialogIsOpen}
        >
          <Container component="main" maxWidth="sm">
            <h1>
              <Button disabled>
                <Typography style={{ fontFamily: "Roboto", color: "#727369" }}>
                  add new ticket
                </Typography>
              </Button>
            </h1>
            <CssBaseline />
            <div>
              <form
                noValidate
                onSubmit={this.onClickSend}
                style={{ marginTop: "30px" }}
              >
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      autoComplete="subject"
                      name="subject"
                      variant="outlined"
                      required
                      fullWidth
                      id="subject"
                      label="subject"
                      autoFocus
                      value={this.state.subject}
                      onChange={this.onChange}
                      error={Boolean(this.state.subjectError.error)}
                      type="text"
                    />
                    {this.state.subjectError.error ? (
                      <div style={{ color: "red" }}>
                        {this.state.subjectError.text}
                        <br />{" "}
                      </div>
                    ) : null}
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      multiline
                      rows={4}
                      id="text"
                      label="context"
                      name="text"
                      autoComplete="text"
                      value={this.state.text}
                      onChange={this.onChange}
                      error={Boolean(this.state.textError.error)}
                      type="text"
                    />
                    {this.state.textError.error ? (
                      <div style={{ color: "red" }}>
                        {this.state.textError.text}
                        <br />{" "}
                      </div>
                    ) : null}
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={2}
                  style={{ width: "100%", margin: "15px" }}
                >
                  <Grid item xs={12} sm={6}>
                    <Button
                      style={{ backgroundColor: "#011627", padding: 10 }}
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                    >
                      send
                    </Button>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Button
                      fullWidth
                      style={{ backgroundColor: "#011627", padding: 10 }}
                      variant="contained"
                      color="primary"
                      onClick={this.onClickCancelTicket.bind(this)}
                    >
                      cancel
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </div>
          </Container>
        </Dialog>
        <Dialog
          style={{ width: "100%", height: "100%" }}
          aria-labelledby="customized-dialog-title"
          open={this.state.addCreditDialogIsOpen}
        >
          <Container component="main" maxWidth="sm">
            <h1>
              <Button disabled>
                <Typography style={{ fontFamily: "Roboto", color: "#727369" }}>
                  add credit
                </Typography>
              </Button>
            </h1>
            <CssBaseline />
            <div>
              <form
                noValidate
                onSubmit={this.onClickAddCredit}
                style={{ marginTop: "30px" }}
              >
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      autoComplete="credit"
                      name="credit"
                      variant="outlined"
                      required
                      fullWidth
                      id="credit"
                      label="credit"
                      autoFocus
                      value={this.state.credit}
                      onChange={this.onChange}
                      error={Boolean(this.state.creditError.error)}
                      type="text"
                    />
                    {this.state.creditError.error ? (
                      <div style={{ color: "red" }}>
                        {this.state.creditError.text}
                        <br />{" "}
                      </div>
                    ) : null}
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={2}
                  style={{ width: "100%", margin: "15px" }}
                >
                  <Grid item xs={12} sm={6}>
                    <Button
                      style={{ backgroundColor: "#011627", padding: 10 }}
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                    >
                      add
                    </Button>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Button
                      fullWidth
                      style={{ backgroundColor: "#011627", padding: 10 }}
                      variant="contained"
                      color="primary"
                      onClick={this.onClickCancelCredit.bind(this)}
                    >
                      cancel
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </div>
          </Container>
        </Dialog>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          open={this.state.snackbarIsOpen}
          onClose={this.handleCloseSnackbar.bind(this)}
          TransitionComponent={SlideTransition}
        >
          <SnackbarContent
            style={{
              backgroundColor: "white",
              border: "2px solid green",
            }}
            message={
              <Button
                disabled
                style={{ backgroundColor: "white", color: "green" }}
                startIcon={
                  <CheckCircleOutlineIcon style={{ color: "green" }} />
                }
                variant="contained"
              >
                {this.state.snackbarMessage}
              </Button>
            }
          />
        </Snackbar>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          open={this.state.snackbar2IsOpen}
          onClose={this.handleCloseSnackbar.bind(this)}
          TransitionComponent={SlideTransition}
        >
          <SnackbarContent
            style={{
              backgroundColor: "white",
              border: "2px solid red",
            }}
            message={
              <Button
                disabled
                style={{ backgroundColor: "white", color: "red" }}
                startIcon={<ErrorOutlineIcon style={{ color: "red" }} />}
                variant="contained"
              >
                {this.state.snackber2Message}
              </Button>
            }
          />
        </Snackbar>
      </TableCell>
    );
  }
}

ProfileSliderLayout.propTypes = {
  logoutUser: Proptypes.func.isRequired,
  clearCart: Proptypes.func.isRequired,
  getProfileInfo: Proptypes.func.isRequired,
  getAccountInfo: Proptypes.func.isRequired,
  editProfile: Proptypes.func.isRequired,
  addCredit: Proptypes.func.isRequired,
  sendTicket: Proptypes.func.isRequired,
  auth: Proptypes.object.isRequired,
  errors: PropType.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});

export default withRouter(
  connect(mapStateToProps, {
    logoutUser,
    clearCart,
    getProfileInfo,
    getAccountInfo,
    editProfile,
    addCredit,
    sendTicket,
  })(ProfileSliderLayout)
);

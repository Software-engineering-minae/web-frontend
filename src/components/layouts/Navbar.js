import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { NavLink } from "react-router-dom";
import { display } from "@material-ui/system";
import { Link } from "@material-ui/core";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { connect } from "react-redux";
import {
  logoutUser,
  getUserInfo,
  checkToken,
  setToken,
} from "../../actions/authActions";
import { withRouter } from "react-router-dom";
import Proptypes from "prop-types";
import SearchField from "react-search-field";
import { useLocation } from "react-router-dom";
import Hidden from "@material-ui/core/Hidden";
import { clearCart } from "../../actions/cartActions";
import axios from "axios";
import createAuthRefreshInterceptor from "axios-auth-refresh";
//const nbgcolor = "#6d597a";
const nbgcolor = "#011627";
//const nbgcolor = "#3c096c";
//const nbgcolor = "#b56576";
//const nbgcolor = "#a37774";

const nbgcolor2 = "#ffffff";

export class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      search: "",
      searchText: "",
    };
  }
  componentDidMount() {
    if (this.props.auth.isAuthenticated === true) {
      axios.defaults.headers = {
        Authorization: "Bearer " + this.props.auth.token,
      };
    }
    if (
      this.props.auth.isAuthenticated === true &&
      this.props.auth.user.username
    ) {
      this.setState({ username: this.props.auth.user.username });
    }

    const refreshAuthLogic = (failedRequest) =>
      axios
        .post("https://hampa2.pythonanywhere.com/accounts/login/refresh/", {
          refresh: this.props.auth.refreshToken,
        })
        .then((tokenRefreshResponse) => {
          // localStorage.setItem('token', tokenRefreshResponse.data.token);

          failedRequest.response.config.headers["Authorization"] =
            "Bearer " + tokenRefreshResponse.data.access;
          this.props.setToken(
            tokenRefreshResponse.data.access,
            this.props.auth.refreshToken
          );
          window.location.reload();
          return Promise.resolve();
        });

    // Instantiate the interceptor (you can chain it as it returns the axios instance)
    createAuthRefreshInterceptor(axios, refreshAuthLogic);
  }
  componentWillReceiveProps() {
    if (this.props.auth.isAuthenticated === true) {
      axios.defaults.headers = {
        Authorization: "Bearer " + this.props.auth.token,
      };
    }
    if (
      this.props.auth.isAuthenticated === true &&
      this.props.auth.user.username
    ) {
      this.setState({ username: this.props.auth.user.username });
    }
  }
  goToProducerSignup() {
    this.props.history.push({ pathname: "/signup/producer" });
  }

  onSearchClick(value, event) {
    console.log(event);
    if (this.state.searchText != "Search") {
      this.setState({ searchText: "Search" });
    } else {
      this.setState({ searchText: "" });
    }

    if (this.props.location.pathname != "/search") {
      this.props.history.push({
        pathname: "/s",
        state: { detail: value, type: "search", page: 1 },
      });
    } else {
      this.props.history.push({
        pathname: "/s",
        state: { detail: value, type: "search" },
      });
    }
  }

  goHome() {
    this.props.history.push("/");
  }
  goSignup() {
    this.props.history.push({ pathname: "/signup/user" });
  }
  async logout() {
    await this.props.logoutUser();
    await this.props.clearCart();
    this.props.history.push({
      pathname: "/s",
      state: { type: "logout" },
    });
  }
  goLogin() {
    this.props.history.push("/login");
  }
  goToProfile() {
    if (this.props.auth.user.is_producer == true) {
      this.props.history.push("/profile/2");
    } else {
      this.props.history.push("/profile/1");
    }
  }
  render() {
    const loginItems = (
      <TableCell align="right" style={{ border: "none", width: "30%" }}>
        <Button
          style={{
            backgroundColor: nbgcolor2,
            color: nbgcolor,
            height: "38px",
          }}
          onClick={this.goSignup.bind(this)}
          variant="outlined"
        >
          SignUp
        </Button>

        <Button
          onClick={this.goLogin.bind(this)}
          style={{
            backgroundColor: nbgcolor2,
            color: nbgcolor,
            marginLeft: "1%",
            height: "38px",
          }}
          variant="outlined"
        >
          LogIn
        </Button>
      </TableCell>
    );
    const logoutItems = (
      <TableCell
        align="right"
        style={{ border: "none", width: "30%", marginRight: "30px" }}
      >
        <Button
          backgroundColor={nbgcolor2}
          color={nbgcolor}
          onClick={this.goToProfile.bind(this)}
          style={{
            color: nbgcolor2,
            cursor: "pointer",
            backgroundColor: nbgcolor,
          }}
        >
          {this.state.username}
        </Button>
        <Button
          style={{
            backgroundColor: nbgcolor2,
            marginLeft: "1%",
            color: nbgcolor,
          }}
          variant="outlined"
          color={nbgcolor}
          onClick={this.logout.bind(this)}
        >
          Logout
        </Button>
      </TableCell>
    );
    return (
      <div style={{ height: "75px", position: "fixed", zIndex: 1000 }}>
        <CssBaseline />
        <AppBar
          style={{
            position: "fixed",
            height: "75px",
            zIndex: "1000",
            backgroundColor: nbgcolor,
          }}
        >
          <Toolbar variant="dense">
            <TableRow
              style={{
                width: "100%",
                display: "flex",
              }}
            >
              <TableCell style={{ border: "none", width: "42%" }}>
                <Typography
                  onClick={this.goHome.bind(this)}
                  // href="/"
                  variant="h5"
                  style={{
                    marginTop: "70px",
                    marginLeft: "30px",
                    width: "100%",
                  }}
                  component="a"
                >
                  <Link
                    underline="none"
                    style={{
                      color: nbgcolor2,
                      cursor: "pointer",
                    }}
                  >
                    HAMPA
                  </Link>
                </Typography>
                {!this.props.auth.isAuthenticated ? (
                  <Hidden xsDown="true">
                    <Button
                      onClick={this.goToProducerSignup.bind(this)}
                      style={{
                        backgroundColor: nbgcolor,
                        color: nbgcolor2,
                        marginLeft: "30px",
                        height: "30px",
                      }}
                    >
                      start your store
                    </Button>
                  </Hidden>
                ) : null}
              </TableCell>

              <TableCell style={{ border: "none", width: "28%" }}>
                <Hidden smDown="true">
                  <SearchField
                    style={{ width: "50%" }}
                    align="center"
                    searchText={this.state.searchText}
                    placeholder="Search"
                    onSearchClick={this.onSearchClick.bind(this)}
                    onEnter={this.onSearchClick.bind(this)}
                  />
                </Hidden>
              </TableCell>

              {this.props.auth.isAuthenticated ? logoutItems : loginItems}
            </TableRow>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

Navbar.propTypes = {
  logoutUser: Proptypes.func.isRequired,
  getUserInfo: Proptypes.func.isRequired,
  clearCart: Proptypes.func.isRequired,
  checkToken: Proptypes.func.isRequired,
  setToken: Proptypes.func.isRequired,
  auth: Proptypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default withRouter(
  connect(mapStateToProps, {
    logoutUser,
    getUserInfo,
    clearCart,
    checkToken,
    setToken,
  })(Navbar)
);

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import BlockIcon from "@material-ui/icons/Block";
import {
  Container,
  Typography,
  CardActionArea,
  TableBody,
} from "@material-ui/core";
import styled from "styled-components";
import React, { useState } from "react";
import PropTypes from "prop-types";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import RemoveShoppingCartIcon from "@material-ui/icons/RemoveShoppingCart";
import { connect } from "react-redux";
import { deleteOrder, addOrder } from "../../actions/cartActions";
import { withRouter } from "react-router-dom";

const El1 = styled.div``;

const El2 = styled.div``;

const El3 = styled.div``;

const HoverdBtn = styled.button`
  transition: all 0.8s linear;
  transform: translate(100%, 100%);
`;
const HoverdDiv = styled.div`
  &:hover {
  }
`;

const HoverdImg = styled.img`
  transition: all 1s linear;
  :hover {
  }
`;
const HoverdContainer = styled.div`
  &:hover {
    ${El1} {
      border: 0.04rem solid rgba(0, 0, 0, 0.2);
      box-shoadow: 2px 2px 5px 0px rgba(0, 0, 0, 0.2);
    }
    ${El2} {
      background: rgba(247, 247, 247);
    }
    ${HoverdBtn} {
      transform: translate(0, 0);
    }
    ${HoverdImg} {
      transform: scale(1.2);
    }
  }
`;

const CardView = (props) => {
  function inCartOnClieck() {
    if (props.product.count > 0) {
      if (inCart == true) {
        props.deleteOrder(props.product.id);
      } else {
        props.addOrder(props.product);
      }
      setInCart(!inCart);
    }
  }
  function initialInCartValue() {
    var result = false;
    var result2 = false;
    props.cart.cart.forEach((order) => {
      props.product.id === order.product.id
        ? (result = true)
        : (result2 = false);
    });
    return result;
  }
  const [inCart, setInCart] = useState(initialInCartValue);
  return (
    <HoverdContainer
      style={{
        margin: "15px",
        width: props.width,
        height: props.height,
        borderRadius: "0.5rem 0.5rem  0.5rem  0.5rem ",
        border: "1px solid #011627",
      }}
    >
      <El1 style={{ borderRadius: "0.5rem 0.5rem  0.5rem  0.5rem " }}>
        <Card
          style={{
            width: props.width,
            height: props.height,
            borderColor: "transparent",
            transition: "all 1s linear",
            borderRadius: "0.5rem 0.5rem  0.5rem  0.5rem ",
          }}
        >
          <CardActionArea
            style={{ borderRadius: 5 }}
            style={{
              width: props.width,
              height: props.height,
              borderRadius: "0.5rem 0.5rem  0.5rem  0.5rem ",
            }}
          >
            <HoverdDiv
              style={{ overflow: "hidden", position: "absolute", top: "0" }}
            >
              <HoverdImg
                onClick={() => {
                  props.history.push({
                    pathname: "/s/",
                    state: { detail: props.product, type: "product" },
                  });
                }}
                style={{ position: "absolute", top: "0" }}
                src={props.src}
                style={{
                  width: props.width,
                  height: (props.height / 400) * 310,
                  borderRadius: "0.5rem 0.5rem  0rem  0rem ",
                }}
                alt="not loaded"
              />
              <HoverdBtn
                onClick={inCartOnClieck}
                className="btn123"
                style={{
                  width: "25%",
                  cursor: "cell",
                  height: "13%",
                  position: "absolute",
                  bottom: 0,
                  right: 0,
                  background: "#d7385e",
                  fontSize: "14px",
                  borderRadius: "0.5rem 0 0 0",
                }}
              >
                {props.product.count > 0 ? (
                  inCart ? (
                    <RemoveShoppingCartIcon style={{ color: "#011627" }} />
                  ) : (
                    <AddShoppingCartIcon style={{ color: "#011627" }} />
                  )
                ) : (
                  <BlockIcon style={{ color: "#011627" }} />
                )}
              </HoverdBtn>
            </HoverdDiv>
            <El2>
              <CardContent
                style={{
                  position: "absolute",
                  left: "3%",
                  top: (props.height / 400) * 300,
                  width: "100%",
                }}
                onClick={() => {
                  props.history.push({
                    pathname: "/s/",
                    state: { detail: props.product, type: "product" },
                  });
                }}
              >
                <p
                  align="left"
                  style={{
                    margin: "1%",
                    fontSize: "15px",
                    color: "#727369",
                    whiteSpace: "nowrap",
                    overflow: "hidden",
                  }}
                >
                  {props.name}
                </p>
                <p
                  align="left"
                  style={{
                    margin: "1%",
                    fontSize: "13px",
                    color: "#727369",
                    whiteSpace: "nowrap",
                    overflow: "hidden",
                  }}
                >
                  price : {props.price} ${" "}
                </p>
              </CardContent>
            </El2>
          </CardActionArea>
        </Card>
      </El1>
    </HoverdContainer>
  );
};

CardView.defaultProps = {
  width: 300,
  height: 400,
  name: "product",
  price: 2000,
  inCart: false,
};
CardView.propTypes = {
  cart: PropTypes.object.isRequired,
  deleteOrder: PropTypes.func.isRequired,
  addOrder: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  cart: state.cart,
  auth: state.auth,
});

export default withRouter(
  connect(mapStateToProps, { deleteOrder, addOrder })(CardView)
);

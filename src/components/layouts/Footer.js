import React from "react";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";

export default function Footer() {
  return (
    <div style={{ width: "100%"  }}>
      <Grid
        container
        justify="space-evenly"
        style={{ backgroundColor: "#ffffff" }}
      >
        <Grid item>
          <Link
            href="#"
            variant="subtitle1"
            style={{ color: "#727369", flexGrow: 1 }}
          ></Link>

          <Typography style={{ color: "#727369", margin: 20 }}>
            © 2019 HAMPA, all rights reserved.
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
}

import React, { Component } from "react";
import { connect } from "react-redux";

class Refresh extends Component {
  componentDidMount() {
    if (this.props.location.state.type == "search") {
      this.props.history.push({
        pathname:
          "/search/" +
          this.props.location.state.detail +
          "/" +
          this.props.location.state.page,
      });
    } else if (this.props.location.state.type == "product") {
      this.props.history.push({
        pathname:
          "/product/" +
          this.props.location.state.detail.id +
          "/" +
          this.props.location.state.detail.title,
      });
    } else if (this.props.location.state.type == "logout") {
      this.props.history.push("/");
    } else if (this.props.location.state.type == "profile") {
      this.props.history.push("/profile/" + this.props.location.state.param);
    } else if (this.props.location.state.type == "products") {
      this.props.history.push({
        pathname:
          "/products/" +
          this.props.location.state.category +
          "/" +
          this.props.location.state.offset,
      });
    }
  }
  render() {
    return <div></div>;
  }
}
export default connect(null, {})(Refresh);

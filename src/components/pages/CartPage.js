import React from "react";
import { Component } from "react";
import { Container, Typography, Grid } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import axios from "axios";
import AddShoppingCartSharpIcon from "@material-ui/icons/AddShoppingCartSharp";
import AddIcon from "@material-ui/icons/Add";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import {
  deleteOrder,
  editOrder,
  refresh,
  addProductToCart,
  submitOrder,
} from "../../actions/cartActions";
import {
  getProfileInfo,
  addNewAddress,
  deleteAddress,
} from "../../actions/authActions";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import LoadingOverlay from "react-loading-overlay";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import Snackbar from "@material-ui/core/Snackbar";
import Slide from "@material-ui/core/Slide";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";

function SlideTransition(props) {
  return <Slide {...props} direction="up" />;
}

const imageURL =
  "https://www.pythonanywhere.com/user/hampa2/files/home/hampa2/";
const listStyle = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
  width: "100%",
};
const mainBgStyle = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
};

const textColor1 = "#727369";
const textColor2 = "#011627";
const bgColor = "#EEEDE7";
const listStyle2 = {
  background: bgColor,
  backgroundColor: bgColor,
  height: "60px",
};
export class CartPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      availableCountDialog: false,
      Cartdata: [],
      isOpen2: false,
      total: 0,
      selectedOption: "option",
      addressWarning: "",
      addresses: [],
      errors: {},
      isOpen: false,
      province: "",
      address: "",
      city: "",
      isLoading: true,
      isLoggedIn: false,
      addressWarning: "",
      availableCountErrors: {},
      selectedOption: "option",
      loading: true,
      snackbarIsOpen: false,
      snackbarMessage: "",
      userProfile: {},
    };
    this.updateInput = this.updateInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.addNewAdddressClicked = this.addNewAdddressClicked.bind(this);
    this.handleAddNewAddress = this.handleAddNewAddress.bind(this);
    this.handleOptionChange = this.handleOptionChange.bind(this);
    this.deleteOrder = this.deleteOrder.bind(this);
  }
  handleCloseSnackbar() {
    this.setState({ snackbarIsOpen: false });
  }
  async closeAvailableCountDialog() {
    await this.setState({ availableCountDialog: false });
  }
  async componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      await this.setState({ isLoggedIn: false });
    } else {
      this.props.refresh();
      await this.setState({ isLoggedIn: true });
      const res2 = await this.props.getProfileInfo();
      await this.setState({
        addresses: res2.data.addresses,
        userProfile: res2.data,
      });
    }
    await this.setState({ Cartdata: this.props.cart.cart, isLoading: false });
    var total = 0;
    this.state.Cartdata.forEach((order) => {
      total = total + order.quantity * order.product.price;
    });
    await this.setState({ total: total, loading: false });
  }
  componentWillReceiveProps(nextProps) {}
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  async updateInput(event) {
    const { id, value } = event.target;
    event.persist();
    if (event.target.value > 0) {
      if (this.state.isLoggedIn) {
        this.props.addProductToCart(id, value);
      }

      this.state.Cartdata.forEach((order) => {
        "" + order.product.id === id
          ? (order.quantity = value)
          : (order.quantity = order.quantity);
      });
    } else {
      if (this.state.isLoggedIn) {
        this.props.addProductToCart(id, 1);
      }
      this.state.Cartdata.forEach((order) => {
        "" + order.product.id === id
          ? (order.quantity = 1)
          : (order.quantity = order.quantity);
      });
    }
    if (value > 0) {
      this.state.Cartdata.forEach((order) => {
        "" + order.product.id === id
          ? (order.quantity = value)
          : (order.quantity = order.quantity);
      });
    } else {
      this.state.Cartdata.forEach((order) => {
        "" + order.product.id === id
          ? (event.target.value = 1)
          : (order.quantity = order.quantity);
      });
    }
    var a = 1;
    this.state.Cartdata.forEach((order) => {
      "" + order.product.id === id ? this.props.editOrder(order) : (a = 2);
    });
    var total = 0;
    this.state.Cartdata.forEach((order) => {
      total = total + order.quantity * order.product.price;
    });
    this.setState({ total: total });
  }

  async deleteOrder(event) {
    await this.props.deleteOrder(event.product.id);
    await this.setState({ Cartdata: this.props.cart.cart });
    var total = 0;
    this.state.Cartdata.forEach((order) => {
      total = total + order.quantity * order.product.price;
    });
    await this.setState({ total: total });
  }

  async handleSubmit() {
    if (this.state.isLoggedIn) {
      if (this.state.selectedOption == "option") {
        this.setState({
          snackbarIsOpen: true,
          snackbarMessage: "you should choose an address",
        });
      } else {
        await this.setState({ availableCountErrors: false });
        this.state.Cartdata.forEach(async (order) => {
          order.product.count < order.quantity
            ? await this.setState({ availableCountErrors: true })
            : await this.setState({
                availableCountErrors: this.state.availableCountErrors,
              });
        });
        if (this.state.availableCountErrors == true) {
          await this.setState({
            snackbarIsOpen: true,
            snackbarMessage: "check your order's count",
          });
        } else {
          const response = await this.props.submitOrder(
            this.state.total,
            this.state.selectedOption
          );
          if (response.data.Message == "Insufficient Credit!") {
            await this.setState({
              snackbarIsOpen: true,
              snackbarMessage: "not enough credit",
            });
          } else {
            if (this.props.auth.user.is_producer == true) {
              this.props.history.push("/profile/2/orders");
            } else {
              this.props.history.push("/profile/1/orders");
            }
          }
        }
      }
    } else {
      this.props.history.push("/login");
    }

    // if (this.state.selectedOption != "option") {
    //   this.state.Cartdata.forEach(async order => {
    //     const res = await axios.post(
    //       "https://hampa.pythonanywhere.com/cart/add/",
    //       JSON.stringify({
    //         workshopID: order.workshop.id,
    //         productID: order.product.id,
    //         designID: order.design.id,
    //         count: order.count,
    //         addressID: this.state.selectedOption
    //       }),
    //       {
    //         headers: {
    //           "Content-Type": "application/json"
    //         }
    //       }
    //     );
    //     console.log(res);
    //     this.props.history.push("/profile/orders");
    //   });
    // } else {
    //   this.setState({ addressWarning: "you should choose an address" });
    // }
  }
  add() {
    this.props.history.push("/");
  }

  async onClickDeleteAddress(id) {
    await this.props.deleteAddress(id);
    const res2 = await this.props.getProfileInfo();
    await this.setState({
      userProfile: res2.data,
      addresses: res2.data.addresses,
    });
  }
  cancelAddressBtn() {
    this.setState({ isOpen: false });
  }
  handleAddNewAddress() {
    this.setState({ isOpen: true, province: "", city: "", address: "" });
  }

  addNewAdddressClicked = async (e) => {
    e.preventDefault();
    await this.props.addNewAddress(
      this.state.province,
      this.state.city,
      this.state.address
    );
    const res2 = await this.props.getProfileInfo();
    await this.setState({
      userProfile: res2.data,
      addresses: res2.data.addresses,
    });
    this.setState({ isOpen: false });
  };

  async handleOptionChange(changeEvent) {
    await this.setState({
      selectedOption: changeEvent.target.value + "",
      addressWarning: "",
    });
  }

  render() {
    const noOrderLayout = (
      <TableCell
        style={{
          height: "100%",
          width: "99%",
          backgroundColor: "#EEEDE7",
          border: "none",
        }}
      >
        <div align="center" style={mainBgStyle}>
          <Container>
            <br></br>
            <TableRow style={{ width: "100%", backgroundColor: "#f4f3f1" }}>
              <TableCell style={{ width: "40%", border: "none" }}>
                <ErrorOutlineIcon
                  style={{
                    margin: "1px",
                    color: "#b7b7a4",
                    width: "300px",
                    height: "300px",
                  }}
                />
              </TableCell>
              <TableCell
                style={{
                  display: "flex",
                  width: "70%",
                  height: "260px",
                  border: "none",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <ul style={{ listStyle: "none", marginTop: "80px" }}>
                  <li>
                    <Typography
                      style={{
                        color: "#a5a5a5",
                        fontSize: "20px",
                        fontWeight: "bold",
                      }}
                    >
                      {" "}
                      unfortunately your cart is empty
                    </Typography>
                  </li>
                  <li style={{ marginTop: "20px" }}>
                    <Typography
                      style={{
                        color: "#a5a5a5",
                        fontSize: "20px",
                        fontWeight: "bold",
                      }}
                    >
                      you can add to it
                    </Typography>
                  </li>
                  <li>
                    <Button
                      onClick={this.add.bind(this)}
                      style={{
                        marginTop: "30px",
                        backgroundColor: "#a5a5a5",
                        cursor: "pointer",
                      }}
                      startIcon={
                        <AddIcon
                          style={{
                            width: "30px",
                            height: "30px",
                            align: "right",
                          }}
                        ></AddIcon>
                      }
                    >
                      add{" "}
                    </Button>
                  </li>
                </ul>
              </TableCell>
            </TableRow>
          </Container>
        </div>
        <div style={{ height: "50px" }}></div>
      </TableCell>
    );

    const addressesLayout = (
      <Grid style={{ width: "1000px", border: "1px solid #EEEDE7" }}>
        <br></br> <br></br> <br></br>
        <TableCell width="20%" style={{ border: "none" }}>
          <div align="left">
            <LocationOnIcon />{" "}
            <span style={{ fontSize: "19px" }}> your address</span>
          </div>
        </TableCell>
        <TableRow>
          <TableCell width="70%" style={{ backgroundColor: "#F1F1F1" }}>
            <FormControl component="fieldset">
              <RadioGroup
                value={this.state.selectedOption}
                onChange={this.handleOptionChange}
              >
                {this.state.addresses.map((address) => (
                  <div className="radio" style={{ marginLeft: "20px" }}>
                    <p style={{ fontSize: "17px" }} />
                    <FormControlLabel
                      value={address.id + ""}
                      control={<Radio style={{ color: textColor1 }} />}
                      label={
                        <span
                          style={{
                            flexGrow: 1,
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          {address.province} / {address.city} /{" "}
                          {address.address}
                        </span>
                      }
                    />

                    {/* <DeleteForeverIcon 
                   onClick={()=>this.onClickDeleteAddress(address.id)}
                   style={{marginRight:"20px" , width :"25px" , height:"25px" , color:textColor2 , cursor:"pointer" }} /> */}
                  </div>
                ))}
              </RadioGroup>
            </FormControl>
          </TableCell>
          <TableCell
            width="30%"
            align="left"
            style={{ backgroundColor: "#F1F1F1", marginLeft: "700px" }}
          >
            <Button
              align="right"
              onClick={this.handleAddNewAddress}
              variant="outlined"
              style={{
                marginLeft: "50px",
                marginRight: "100px",

                backgroundColor: "#f1f1f1",
              }}
            >
              <AddIcon />
              <span style={{ fontSize: "12px" }}>new address</span>
            </Button>
          </TableCell>
        </TableRow>
      </Grid>
    );
    const noAddressLayout = (
      <Grid style={{ width: "1000px", border: "1px solid #EEEDE7" }}>
        <br></br> <br></br> <br></br>
        <TableRow>
          <TableCell width="20%" style={{ backgroundColor: "#F1F1F1" }}>
            <div align="left">
              <LocationOnIcon />{" "}
              <span style={{ fontSize: "19px" }}> your address</span>
            </div>
          </TableCell>
          <TableCell width="50%" style={{ backgroundColor: "#F1F1F1" }}>
            <form style={{ marginLeft: "30px", backgroundColor: "#F1F1F1" }}>
              {this.state.addresses.map((address) => (
                <div className="radio">
                  <label>
                    <p type="radio" value={address.id} checked={true} />
                    {address.city}:{address.address}
                  </label>
                </div>
              ))}
            </form>
          </TableCell>
          <TableCell
            width="30%"
            align="left"
            style={{ backgroundColor: "#F1F1F1", marginLeft: "700px" }}
          >
            <Button
              align="right"
              onClick={this.handleAddNewAddress}
              variant="outlined"
              style={{
                marginLeft: "50px",
                marginRight: "100px",

                backgroundColor: "#f1f1f1",
              }}
            >
              <AddIcon />
              <span style={{ fontSize: "12px" }}>new address</span>
            </Button>
          </TableCell>
        </TableRow>
      </Grid>
    );
    return (
      <div align="center" style={listStyle}>
        <LoadingOverlay
          active={this.state.loading}
          spinner
          text="please wait ..."
        >
          <div style={listStyle2}>
            <Button
              disabled
              align="center"
              variant="contained"
              style={{
                align: "center",
                marginTop: "10px",
                color: "#011627",
                fontSize: "20px",
                backgroundColor: bgColor,
              }}
            >
              shopping cart{" "}
            </Button>
          </div>
          <br></br>

          <Container align="center" style={listStyle}>
            {this.state.isLoading === true ? (
              <div style={{ height: window.innerHeight }}></div>
            ) : this.state.Cartdata.length === 0 ? (
              noOrderLayout
            ) : (
              <Paper>
                <Table aria-label="orders">
                  <TableHead
                    style={{ color: "#e9e9e3", backgroundColor: "#f9f9f9" }}
                  >
                    <TableRow>
                      <TableCell align="center">
                        <h1 style={{ fontSize: "18px" }}>
                          {this.state.Cartdata.length === 0 ? null : "orders"}{" "}
                        </h1>
                      </TableCell>
                      <TableCell align="center">
                        <h1 style={{ fontSize: "15px" }}></h1>
                      </TableCell>
                      <TableCell align="center">
                        <h1 style={{ fontSize: "15px" }}>
                          {this.state.Cartdata.length === 0 ? null : "Count"}{" "}
                        </h1>
                      </TableCell>
                      <TableCell align="center">
                        <h1 style={{ fontSize: "15px" }}></h1>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.Cartdata.map((order) => (
                      <TableRow
                        key={order.product.id}
                        style={{ backgroundColor: "#f9f9f9" }}
                      >
                        <TableCell align="center" width="20%">
                          <img
                            src={imageURL + order.product.image}
                            style={{ width: "200px", height: "200px" }}
                          />
                        </TableCell>
                        <TableCell width="60%" align="left">
                          <ul style={{ listStyle: "none" }}>
                            <li style={{ margin: "10px", fontSize: "22px" }}>
                              {order.product.title}
                            </li>
                            <li
                              style={{
                                margin: "10px",
                                fontSize: "19px",
                                color: "#727369",
                              }}
                            >
                              {order.product.detail}
                            </li>
                            <li
                              style={{
                                margin: "20px",
                                marginLeft: "10px",
                                fontSize: "17px",
                                color: "#727369",
                              }}
                            >
                              {" "}
                              price : {order.product.price} $
                            </li>
                            <li
                              style={{
                                margin: "20px",
                                marginLeft: "10px",
                                fontSize: "17px",
                                color: "#727369",
                              }}
                            >
                              {" "}
                              available count : {order.product.count}
                            </li>
                          </ul>
                        </TableCell>
                        <TableCell width="10%" align="center">
                          <TextField
                            id={order.product.id}
                            type="number"
                            name="title"
                            variant="outlined"
                            style={{ width: "60px" }}
                            defaultValue={order.quantity}
                            onChange={this.updateInput}
                            error={order.product.count < order.quantity}
                          />
                          {order.product.count < order.quantity ? (
                            <div style={{ color: "red" }}>
                              {"available count is less than your request"}
                              <br />{" "}
                            </div>
                          ) : null}
                        </TableCell>
                        <TableCell width="10%">
                          <DeleteForeverIcon
                            id={order.quantity.id}
                            onClick={() => this.deleteOrder(order)}
                            style={{
                              width: "25px",
                              height: "25px",
                              marginLeft: "15px",
                              align: "right",
                              cursor: "pointer",
                            }}
                          ></DeleteForeverIcon>
                        </TableCell>
                      </TableRow>
                    ))}

                    <TableRow style={{ backgroundColor: "#f9f9f9" }}>
                      <TableCell style={{ width: "20%", border: "none" }}>
                        {this.state.isLoggedIn ? (
                          <h1 style={{ fontSize: "18px", color: "#727369" }}>
                            credit : {this.state.userProfile.credit}
                          </h1>
                        ) : null}
                      </TableCell>
                      <TableCell style={{ width: "60%", border: "none" }}>
                        <h1 style={{ fontSize: "20px" }} align="center">
                          total {this.state.total}$
                        </h1>
                      </TableCell>

                      <TableCell
                        width="10%"
                        style={{ border: "none" }}
                      ></TableCell>
                      <TableCell width="10%" style={{ border: "none" }}>
                        <AddIcon
                          onClick={this.add.bind(this)}
                          cursor="pointer"
                          style={{
                            width: "50px",
                            height: "50px",
                            align: "right",
                          }}
                        >
                          add
                        </AddIcon>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </Paper>
            )}

            {this.state.isLoading === true || this.state.Cartdata.length === 0
              ? null
              : this.state.isLoggedIn === true
              ? this.state.addresses.length === 0
                ? noAddressLayout
                : addressesLayout
              : null}

            {this.state.Cartdata.length === 0 ? null : (
              <Grid item style={{ marginTop: "20px" }}>
                <h1 style={{ color: "red", fontSize: "12px" }}>
                  {this.state.addressWarning}
                </h1>
                <Button
                  onClick={this.handleSubmit}
                  variant="outlined"
                  size="medium"
                  style={{
                    marginRight: "20px",
                    color: "#ffffff",
                    backgroundColor: "#011627",
                  }}
                >
                  {this.state.isLoggedIn ? "submit" : "login first"}
                </Button>
              </Grid>
            )}

            <Dialog
              style={{ width: "100%", height: "100%" }}
              aria-labelledby="customized-dialog-title"
              open={this.state.isOpen}
            >
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  backgroundColor: "#ffffff",
                }}
              >
                <form
                  noValidate
                  onSubmit={this.addNewAdddressClicked}
                  style={{ margin: "20px" }}
                >
                  <TextField
                    margin="20px"
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="province"
                    label="province"
                    id="province"
                    autoFocus
                    onChange={this.onChange}
                  />
                  <TextField
                    margin="20px"
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="city"
                    label="city"
                    name="city"
                    autoFocus
                    onChange={this.onChange}
                  />

                  <TextField
                    margin="20px"
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="address"
                    label="address"
                    id="address"
                    autoFocus
                    onChange={this.onChange}
                  />
                  <Grid container spacing={2} style={{ width: "100%" }}>
                    <Grid item xs={12} sm={6}>
                      <Button
                        style={{ backgroundColor: "#011627", padding: 10 }}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                      >
                        add
                      </Button>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Button
                        fullWidth
                        style={{ backgroundColor: "#011627", padding: 10 }}
                        variant="contained"
                        color="primary"
                        onClick={this.cancelAddressBtn.bind(this)}
                      >
                        cancel
                      </Button>
                    </Grid>
                  </Grid>
                </form>
              </div>
            </Dialog>
            <Dialog
              open={this.state.availableCountDialog}
              onClick={this.closeAvailableCountDialog.bind(this)}
            >
              <div style={{ margin: "20px" }}>
                <TableRow>
                  <TableCell style={{ border: "none", width: "20%" }}>
                    <ErrorOutlineIcon
                      style={{
                        width: "80px",
                        height: "80px",
                        color: "#d7385e",
                      }}
                    ></ErrorOutlineIcon>
                  </TableCell>
                  <TableCell style={{ border: "none", width: "80%" }}>
                    <h1 style={{ margin: "40px", color: "#d7385e" }}>
                      unfortunately available count of your orders is less than
                      your request{" "}
                    </h1>
                  </TableCell>
                </TableRow>
              </div>
            </Dialog>
          </Container>
          <div style={{ height: "350px" }}></div>
        </LoadingOverlay>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          open={this.state.snackbarIsOpen}
          onClose={this.handleCloseSnackbar.bind(this)}
          TransitionComponent={SlideTransition}
        >
          <SnackbarContent
            style={{
              backgroundColor: "white",
              border: "2px solid red",
            }}
            message={
              <Button
                disabled
                style={{ backgroundColor: "white", color: "red" }}
                startIcon={<ErrorOutlineIcon style={{ color: "red" }} />}
                variant="contained"
              >
                {this.state.snackbarMessage}
              </Button>
            }
          />
        </Snackbar>
      </div>
    );
  }
}

CartPage.propTypes = {
  cart: PropTypes.object.isRequired,
  deleteOrder: PropTypes.func.isRequired,
  refresh: PropTypes.func.isRequired,
  editOrder: PropTypes.func.isRequired,
  submitOrder: PropTypes.func.isRequired,
  getProfileInfo: PropTypes.func.isRequired,
  addNewAddress: PropTypes.func.isRequired,
  deleteAddress: PropTypes.func.isRequired,
  addProductToCart: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  cart: state.cart,
  auth: state.auth,
});

export default connect(mapStateToProps, {
  deleteOrder,
  editOrder,
  refresh,
  addProductToCart,
  submitOrder,
  getProfileInfo,
  addNewAddress,
  deleteAddress,
})(CartPage);

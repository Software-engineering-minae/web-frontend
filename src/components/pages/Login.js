import React, { Component } from "react";
import PropType from "prop-types";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { loginUser } from "../../actions/authActions";
import axios from "axios";
import { deleteOrder, addOrder , refresh , addProductToCart } from "../../actions/cartActions";
import PropTypes from "prop-types";

const bgColor="#011627"

export class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      errors: {}
    };
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
    
      this.props.cart.cart.forEach(async (order) => {
        this.props.addProductToCart(order.product.id,order.quantity) ;
      })
      this.props.refresh();


      this.props.history.push("/");
    }
  }
  goToSignup() {
    this.props.history.push({pathname:"/signup/user" });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  async componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      nextProps.cart.cart.forEach(async (order) => {
        this.props.addProductToCart(order.product.id,order.quantity) ;
        
      })
      nextProps.refresh();



      nextProps.history.push("/");
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit = async e => {
    e.preventDefault();
    const userData = {
      username: this.state.username,
      password: this.state.password
    };

    await this.props.loginUser(userData);
  };

  render() {
    return (
      <Container component="main" maxWidth="sm">
      <CssBaseline />
      <div>
        <Container align="center">
            <AccountCircleIcon
              align="center"
              style={{ width: "200px", height: "200px", align: "center"  , color:bgColor}}
            />
            
          </Container>
          <form noValidate onSubmit={this.onSubmit} style={{marginTop:"30px"}}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoFocus
              onChange={this.onChange}
              error={this.props.errors.username}
            />
            {this.props.errors.username ? (
              <div style={{ color: "red" }}>
                {this.props.errors.username}
                <br />{" "}
              </div>
            ) : null}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              autoFocus
              onChange={this.onChange}
              error={this.props.errors.password}
            />
            {this.props.errors.password ? (
              <div style={{ color: "red" }}>
                {this.props.errors.password}
                <br />{" "}
              </div>
            ) : null}
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              style={{ backgroundColor: bgColor, padding: 10 }}
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
            >
              Login
            </Button>
            <Grid container>
              <Grid item>
                <Link variant="body2">
                  <span style={{ color: "#011627" }}>
                    <Typography
                      onClick={this.goToSignup.bind(this)}
                      style={{
                        marginTop: 5,
                        fontSize: "10pt",
                        marginLeft: 20
                      }}
                    >
                      "Don't have an account? Sign Up"
                    </Typography>
                  </span>
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <div>
          {this.props.errors.UserError ? (
            <div style={{ color: "red" }}>
              Your username or password is incorrect
              <br />{" "}
            </div>
          ) : null}
        </div>
      </Container>
    );
  }
}

Login.propTypes = {
  loginUser: PropType.func.isRequired,
  auth: PropType.object.isRequired,
  errors: PropType.object.isRequired,
  cart: PropTypes.object.isRequired,
  deleteOrder: PropTypes.func.isRequired,
  addProductToCart: PropTypes.func.isRequired,
  addOrder: PropTypes.func.isRequired,
  refresh: PropTypes.func.isRequired,

};

const mapStateToProps = state => ({
  auth: state.auth,
  cart: state.cart,
  errors: state.errors
});

export default connect(mapStateToProps, { loginUser , deleteOrder,addOrder,refresh , addProductToCart })(Login);

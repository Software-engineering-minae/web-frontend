import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TableCell from "@material-ui/core/TableCell";
import { Container } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TableRow from "@material-ui/core/TableRow";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import StoreIcon from "@material-ui/icons/Store";
import { Link } from "@material-ui/core";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Hidden from "@material-ui/core/Hidden";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ProfileSliderLayout from "../layouts/ProfileSliderLayout";
import Axios from "axios";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import Dialog from "@material-ui/core/Dialog";
import { getOrders } from "../../actions/cartActions";
import { Item } from "semantic-ui-react";
import MoreIcon from "@material-ui/icons/More";
import CachedIcon from "@material-ui/icons/Cached";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import MotorcycleIcon from "@material-ui/icons/Motorcycle";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import LoadingOverlay from "react-loading-overlay";

const imageURL =
  "https://www.pythonanywhere.com/user/hampa2/files/home/hampa2/";

const bgColor2 = "#ffffff";
const bgColor = "#011627";
const mainBgStyle = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
};

const secondaryBgStyle = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
  height: "60px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

class ProfileOrdersPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogIsOpen: false,
      orderData: { order_items: [] },
      ordersData: [],
      isLoading: true,
      loading: true,
      bgColor: "#f9f9f9",
    };
  }

  async mouseEnterRating(id) {
    let ordersData = this.state.ordersData;
    ordersData.forEach((order) => {
      order.id === id
        ? (order.bgColor = "#f4f3f1")
        : (order.bgColor = "#f9f9f9");
    });
    await this.setState({ ordersData: ordersData });
  }
  async mouseLeaveRating(id) {
    var ordersData = this.state.ordersData;
    ordersData.forEach((order) => {
      order.id === id
        ? (order.bgColor = "#f9f9f9")
        : (order.bgColor = "#f9f9f9");
    });
    await this.setState({ ordersData: ordersData });
  }
  async componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      this.props.history.push("/login");
    }
    const orders = await this.props.getOrders();

    orders.data.forEach((order) => {
      order.bgColor = "#f9f9f9";
    });
    await this.setState({
      ordersData: orders.data.reverse(),
      isLoading: false,
      loading: false,
    });
  }
  async showDetail(order) {
    this.setState({ orderData: order, dialogIsOpen: true });
  }
  onCloseDialog() {
    this.setState({ dialogIsOpen: false });
  }
  render() {
    const dialog = (
      <Dialog
        open={this.state.dialogIsOpen}
        onClose={this.onCloseDialog.bind(this)}
      >
        <div style={{ margin: "50px" }}>
          <Table aria-label="orders">
            <TableHead style={{ color: "#e9e9e3", backgroundColor: "#f9f9f9" }}>
              <TableRow>
                <TableCell align="left"></TableCell>
                <TableCell align="center">
                  <h1 style={{ fontSize: "15px" }}></h1>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.orderData.order_items.map((order) => (
                <TableRow key={order.product.id}>
                  <TableCell align="center" width="20%">
                    <img
                      src={imageURL + order.product.image}
                      style={{ width: "200px", height: "200px" }}
                    />
                  </TableCell>
                  <TableCell width="60%" align="left">
                    <ul style={{ listStyle: "none" }}>
                      <li style={{ margin: "10px", fontSize: "22px" }}>
                        {order.product.title}
                      </li>
                      <li
                        style={{
                          margin: "10px",
                          fontSize: "19px",
                          color: "#727369",
                        }}
                      >
                        {order.product.detail}
                      </li>
                      <li
                        style={{
                          margin: "20px",
                          marginLeft: "10px",
                          fontSize: "17px",
                          color: "#727369",
                        }}
                      >
                        {" "}
                        price : {order.product.price} $
                      </li>
                      <li
                        style={{
                          margin: "20px",
                          marginLeft: "10px",
                          fontSize: "17px",
                          color: "#727369",
                        }}
                      >
                        {" "}
                        count : {order.quantity}
                      </li>
                    </ul>
                  </TableCell>
                </TableRow>
              ))}
              <TableRow></TableRow>
            </TableBody>
          </Table>
        </div>
      </Dialog>
    );
    const noOrderLayout = (
      <TableCell
        style={{
          height: "100%",
          width: "99%",
          backgroundColor: "#EEEDE7",
          border: "none",
        }}
      >
        <div align="center" style={mainBgStyle}>
          <Container>
            <br></br>
            <TableRow style={{ width: "100%", backgroundColor: "#f4f3f1" }}>
              <TableCell style={{ width: "40%", border: "none" }}>
                <ErrorOutlineIcon
                  style={{
                    margin: "1px",
                    color: "#b7b7a4",
                    width: "300px",
                    height: "300px",
                  }}
                />
              </TableCell>
              <TableCell
                style={{
                  display: "flex",
                  width: "70%",
                  height: "260px",
                  border: "none",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <ul style={{ listStyle: "none" }}>
                  <li>
                    <Typography
                      style={{
                        color: "#a5a5a5",
                        fontSize: "20px",
                        fontWeight: "bold",
                      }}
                    >
                      {" "}
                      you dont have any order yet
                    </Typography>
                  </li>
                  <li style={{ marginTop: "20px" }}>
                    <Typography
                      style={{
                        color: "#a5a5a5",
                        fontSize: "20px",
                        fontWeight: "bold",
                      }}
                    >
                      please add your first order
                    </Typography>
                  </li>
                </ul>
              </TableCell>
            </TableRow>
          </Container>
        </div>
      </TableCell>
    );

    const ordersLayout = (
      <TableCell
        style={{
          height: "100%",
          width: "99%",
          backgroundColor: "#EEEDE7",
          border: "none",
        }}
      >
        <div align="center" style={mainBgStyle}>
          <Container>
            <br></br>
            <Container>
              <Table aria-label="orders">
                <TableHead
                  style={{ color: "#e9e9e3", backgroundColor: "#f9f9f9" }}
                >
                  <TableRow>
                    <TableCell style={{ width: "10%" }} align="center">
                      <h1 style={{ fontSize: "15px" }}>items count</h1>
                    </TableCell>
                    <TableCell style={{ width: "20%" }} align="center">
                      <h1 style={{ fontSize: "15px" }}>date</h1>
                    </TableCell>
                    <TableCell style={{ width: "15%" }} align="center">
                      <h1 style={{ fontSize: "15px" }}>total cost</h1>
                    </TableCell>
                    <TableCell style={{ width: "25%" }} align="left">
                      <h1 style={{ fontSize: "15px", marginLeft: "40px" }}>
                        items
                      </h1>
                    </TableCell>
                    <TableCell style={{ width: "15%" }} align="center">
                      <h1 style={{ fontSize: "15px" }}>status</h1>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.ordersData.map((order) => (
                    <TableRow
                      key={order.id}
                      onMouseEnter={() => this.mouseEnterRating(order.id)}
                      onMouseLeave={() => this.mouseLeaveRating(order.id)}
                      onClick={() => this.showDetail(order)}
                      style={{
                        minHeight: "100px",
                        backgroundColor: order.bgColor,
                        cursor: "pointer",
                      }}
                    >
                      <TableCell align="center" style={{ width: "15%" }}>
                        <span style={{ marginLeft: "20px" }}>
                          {order.order_items.length}
                        </span>
                      </TableCell>
                      <TableCell align="center" style={{ width: "20%" }}>
                        {order.created_at.split("T")[0]}
                      </TableCell>
                      <TableCell align="center" style={{ width: "15%" }}>
                        {order.total} $
                      </TableCell>
                      <TableCell align="left" style={{ width: "25%" }}>
                        <span style={{ marginLeft: "40px" }}>
                          {order.order_items.map((item) => (
                            <TableRow> ⚫ {item.product.title} </TableRow>
                          ))}
                        </span>
                      </TableCell>
                      <TableCell align="center" style={{ width: "20%" }}>
                        {order.status === false ? (
                          <Button
                            startIcon={<CachedIcon />}
                            variant="outlined"
                            style={{
                              marginRight: "20px",
                              border: "none",
                              color: "red",
                            }}
                            disabled
                          >
                            pending
                          </Button>
                        ) : (
                          <Button
                            startIcon={<CheckCircleOutlineIcon />}
                            variant="outlined"
                            style={{
                              marginRight: "20px",
                              border: "none",
                              color: "green",
                            }}
                            disabled
                          >
                            deliverd
                          </Button>
                        )}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Container>
          </Container>
        </div>
      </TableCell>
    );

    return (
      <div className={mainBgStyle} style={mainBgStyle}>
        {/* <div style={secondaryBgStyle}>
          <Button
            disabled
            align="center"
            variant="contained"
            style={{
              align: "center",
              marginTop: "10px",
              fontSize: "20px",
              backgroundColor: "#EEEDE7",
              color: bgColor
            }}
          >
            your orders
          </Button>
        </div> */}
        <LoadingOverlay
          active={this.state.loading}
          spinner
          text="please wait ..."
        >
          <TableRow
            style={{
              height: "100%",
              width: "100%",
              backgroundColor: "#EEEDE7",
            }}
          >
            <Hidden mdDown="true">
              <ProfileSliderLayout type={this.props.match.params.type} />
            </Hidden>
            {this.state.isLoading === false
              ? this.state.ordersData.length === 0
                ? noOrderLayout
                : ordersLayout
              : null}

            {dialog}
          </TableRow>
          <div
            style={{
              height: this.props.match.params.type == 1 ? "300px" : "200px",
              backgroundColor: "#EEEDE7",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          ></div>
        </LoadingOverlay>
      </div>
    );
  }
}

ProfileOrdersPage.propTypes = {
  auth: PropTypes.object.isRequired,
  getOrders: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { getOrders })(ProfileOrdersPage);

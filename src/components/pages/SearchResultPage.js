import { Component } from "react";
import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { Container, Typography, CardActionArea } from "@material-ui/core";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import axios from "axios";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Search from "@material-ui/icons/Search";
import Button from "@material-ui/core/Button";
import CardView from "../layouts/CardView";
import CartLayout from "../layouts/CartLayout";
import {
  search,
  searchPage,
  searchTotalPages,
} from "../../actions/productsActions";
import LoadingOverlay from "react-loading-overlay";
import Pagination from "material-ui-flat-pagination";

const imageURL =
  "https://www.pythonanywhere.com/user/hampa2/files/home/hampa2/";
const listStyle3 = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
};

const listStyle4 = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
};

const bgColor = "#EEEDE7";
const listStyle2 = {
  background: bgColor,
  backgroundColor: bgColor,
  height: "60px",
};

class SearchResultPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      search: "",
      text: "",
      loading: true,
      isLoading: true,
      offset: 1,
      totolProducts: 0,
      pageSize: 8,
    };
  }
  async handleClick(offset) {
    this.props.history.push({
      pathname: "/s",
      state: { detail: this.state.text, type: "search", page: offset / 8 + 1 },
    });
  }
  async componentDidMount() {
    const { text, page } = this.props.match.params;
    await this.setState({ text: text, offset: (page - 1) * 8 });
    const res = await this.props.searchPage(text, page);
    const res2 = await this.props.searchTotalPages(text);

    this.setState({
      totolProducts: res2.data.product_number,
      data: res.data,
      loading: false,
    });
  }

  render() {
    return (
      <div align="center" style={listStyle4}>
        <LoadingOverlay
          active={this.state.loading}
          spinner
          text="please wait ..."
        >
          <div style={listStyle2}>
            <Button
              disabled
              align="center"
              variant="contained"
              style={{
                align: "center",
                marginTop: "10px",
                color: "#011627",
                fontSize: "20px",
                backgroundColor: bgColor,
              }}
            >
              you searched for {this.state.text} and we have{" "}
              {this.state.totolProducts} results for you{" "}
            </Button>
          </div>
          <br></br>
          <Container align="center" style={listStyle3}>
            <GridList cols={4}>
              {this.state.data.map((product) => (
                <CardView
                  width={270}
                  height={350}
                  name={product.title}
                  price={product.price}
                  product={product}
                  src={imageURL + product.image}
                />
              ))}
            </GridList>
          </Container>
          <CartLayout />
          {this.state.data.length != 0 ? (
            <div
              style={{
                height:
                  window.innerHeight -
                    Math.ceil(this.state.data.length / 4) * 400 -
                    200 >
                  0
                    ? window.innerHeight -
                      Math.ceil(this.state.data.length / 4) * 400 -
                      200
                    : 0,
              }}
            ></div>
          ) : (
            <div
              style={{
                height:
                  window.innerHeight -
                    parseInt(this.state.data.length / 4) * 400 -
                    200 >
                  0
                    ? window.innerHeight -
                      parseInt(this.state.data.length / 4) * 400 -
                      200
                    : 0,
              }}
            ></div>
          )}
          <Pagination
            style={{
              justifyContent: "center",
              alignItems: "center",
              align: "center",
              marginTop: "20px",
            }}
            limit={this.state.pageSize}
            offset={this.state.offset}
            total={this.state.totolProducts}
            onClick={(e, offset) => this.handleClick(offset)}
          />
          <div style={{ height: "20px" }}></div>
        </LoadingOverlay>
      </div>
    );
  }
}

SearchResultPage.propTypes = {
  search: PropTypes.func.isRequired,
  searchPage: PropTypes.func.isRequired,
  searchTotalPages: PropTypes.func.isRequired,
};
export default connect(null, { search, searchTotalPages, searchPage })(
  SearchResultPage
);

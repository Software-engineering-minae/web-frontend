import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TableCell from "@material-ui/core/TableCell";
import { Container } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TableRow from "@material-ui/core/TableRow";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import StoreIcon from "@material-ui/icons/Store";
import { Link } from "@material-ui/core";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Hidden from "@material-ui/core/Hidden";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ProfileSliderLayout from "../layouts/ProfileSliderLayout";
import Axios from "axios";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import Dialog from "@material-ui/core/Dialog";
import {
  getSalesRecords,
  getOrders,
  changeStatus,
} from "../../actions/cartActions";
import { Item } from "semantic-ui-react";
import MoreIcon from "@material-ui/icons/More";
import CachedIcon from "@material-ui/icons/Cached";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import MotorcycleIcon from "@material-ui/icons/Motorcycle";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import ReactLoading from "react-loading";
import LoadingOverlay from "react-loading-overlay";

const imageURL =
  "https://www.pythonanywhere.com/user/hampa2/files/home/hampa2/";

const bgColor2 = "#f9f9f9";
const bgColor = "#011627";
const mainBgStyle = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
};

const secondaryBgStyle = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
  height: "60px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

class ProfileOrdersPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogIsOpen: false,
      orderData: { order_items: [] },
      ordersData: [],
      isLoading: true,
      loading: true,
    };
  }

  async mouseEnterRating(id) {
    let ordersData = this.state.ordersData;
    ordersData.forEach((order) => {
      order === id ? (order.bgColor = "#f4f3f1") : (order.bgColor = "#f9f9f9");
    });
    await this.setState({ ordersData: ordersData });
  }
  async mouseLeaveRating(id) {
    var ordersData = this.state.ordersData;
    ordersData.forEach((order) => {
      order === id ? (order.bgColor = "#f9f9f9") : (order.bgColor = "#f9f9f9");
    });
    await this.setState({ ordersData: ordersData });
  }

  async handleChangeCat(event) {
    await this.setState({ loading: true });
    let items = [];
    let itemsID = [];
    this.state.ordersData.forEach((order) => {
      order === event.target.name
        ? (items = order.order_items)
        : (items = items);
    });
    items.forEach((item) => {
      itemsID = [...itemsID, item.id];
    });
    await this.props.changeStatus(itemsID, !items[0].status);
    const orders = await this.props.getSalesRecords();
    orders.data.forEach((order) => {
      order.bgColor = "#f9f9f9";
      order.total = 0;
      order.order_items.forEach((item) => {
        order.total += Number(item.product.price);
      });
    });
    await this.setState({
      ordersData: orders.data.reverse(),
      loading: false,
    });
  }
  async componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      this.props.history.push("/login");
    }
    const orders = await this.props.getSalesRecords();
    orders.data.forEach((order) => {
      order.bgColor = "#f9f9f9";
      order.total = 0;
      order.order_items.forEach((item) => {
        order.total += Number(item.product.price);
      });
    });
    await this.setState({
      ordersData: orders.data.reverse(),
      isLoading: false,
      loading: false,
    });
  }
  async showDetail(order) {
    this.setState({ orderData: order, dialogIsOpen: true });
  }
  onCloseDialog() {
    this.setState({ dialogIsOpen: false });
  }
  render() {
    const dialog = (
      <Dialog
        open={this.state.dialogIsOpen}
        onClose={this.onCloseDialog.bind(this)}
      >
        <div style={{ margin: "50px" }}>
          <Table aria-label="orders">
            <TableHead style={{ color: "#e9e9e3", backgroundColor: "#f9f9f9" }}>
              <TableRow>
                <TableCell align="left"></TableCell>
                <TableCell align="center">
                  <h1 style={{ fontSize: "15px" }}></h1>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.orderData.order_items.map((order) => (
                <TableRow key={order.product.id}>
                  <TableCell align="center" width="20%">
                    <img
                      src={imageURL + order.product.image}
                      style={{ width: "200px", height: "200px" }}
                    />
                  </TableCell>
                  <TableCell width="60%" align="left">
                    <ul style={{ listStyle: "none" }}>
                      <li style={{ margin: "10px", fontSize: "22px" }}>
                        {order.product.title}
                      </li>
                      <li
                        style={{
                          margin: "10px",
                          fontSize: "19px",
                          color: "#727369",
                        }}
                      >
                        {order.product.detail}
                      </li>
                      <li
                        style={{
                          margin: "20px",
                          marginLeft: "10px",
                          fontSize: "17px",
                          color: "#727369",
                        }}
                      >
                        {" "}
                        price : {order.product.price} $
                      </li>
                      <li
                        style={{
                          margin: "20px",
                          marginLeft: "10px",
                          fontSize: "17px",
                          color: "#727369",
                        }}
                      >
                        {" "}
                        count : {order.quantity}
                      </li>
                    </ul>
                  </TableCell>
                </TableRow>
              ))}
              <TableRow></TableRow>
            </TableBody>
          </Table>
        </div>
      </Dialog>
    );
    const noOrderLayout = (
      <TableCell
        style={{
          height: "100%",
          width: "99%",
          backgroundColor: "#EEEDE7",
          border: "none",
        }}
      >
        <div align="center" style={mainBgStyle}>
          <Container>
            <br></br>
            <TableRow style={{ width: "100%", backgroundColor: "#f4f3f1" }}>
              <TableCell style={{ width: "40%", border: "none" }}>
                <ErrorOutlineIcon
                  style={{
                    margin: "1px",
                    color: "#b7b7a4",
                    width: "300px",
                    height: "300px",
                  }}
                />
              </TableCell>
              <TableCell
                style={{
                  display: "flex",
                  width: "70%",
                  height: "260px",
                  border: "none",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <ul style={{ listStyle: "none" }}>
                  <li>
                    <Typography
                      style={{
                        color: "#a5a5a5",
                        fontSize: "20px",
                        fontWeight: "bold",
                      }}
                    >
                      {" "}
                      Unfortunately no orders have
                    </Typography>
                  </li>
                  <li style={{ marginTop: "20px" }}>
                    <Typography
                      style={{
                        color: "#a5a5a5",
                        fontSize: "20px",
                        fontWeight: "bold",
                      }}
                    >
                      been submited so far
                    </Typography>
                  </li>
                </ul>
              </TableCell>
            </TableRow>
          </Container>
        </div>
      </TableCell>
    );

    const ordersLayout = (
      <TableCell
        style={{
          height: "100%",
          width: "99%",
          backgroundColor: "#EEEDE7",
          border: "none",
        }}
      >
        <div align="center" style={mainBgStyle}>
          <Container>
            <br></br>
            <Container>
              <Table aria-label="orders">
                <TableHead
                  style={{ color: "#e9e9e3", backgroundColor: "#f9f9f9" }}
                >
                  <TableRow>
                    <TableCell style={{ width: "2%" }} align="center">
                      <h1 style={{ fontSize: "15px" }}></h1>
                    </TableCell>
                    <TableCell style={{ width: "20%" }} align="left">
                      <h1 style={{ fontSize: "15px" }}>items</h1>
                    </TableCell>

                    <TableCell style={{ width: "10%" }} align="center">
                      <h1 style={{ fontSize: "15px" }}>date</h1>
                    </TableCell>
                    <TableCell style={{ width: "15%" }} align="center">
                      <h1 style={{ fontSize: "15px" }}>total cost</h1>
                    </TableCell>

                    <TableCell style={{ width: "43%" }} align="left">
                      <h1 style={{ fontSize: "15px", marginLeft: "10px" }}>
                        detail
                      </h1>
                    </TableCell>
                    <TableCell style={{ width: "10" }} align="center">
                      <h1 style={{ fontSize: "15px" }}>status</h1>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.ordersData.map((order) => (
                    <TableRow
                      onMouseEnter={() => this.mouseEnterRating(order)}
                      onMouseLeave={() => this.mouseLeaveRating(order)}
                      key={order.id}
                      style={{
                        minHeight: "100px",
                        backgroundColor: order.bgColor,
                      }}
                    >
                      <TableCell
                        style={{ width: "2%", cursor: "pointer" }}
                        align="center"
                        onClick={() => this.showDetail(order)}
                      >
                        <h1 style={{ fontSize: "15px" }}></h1>
                      </TableCell>
                      <TableCell
                        align="left"
                        style={{ width: "20%", cursor: "pointer" }}
                        onClick={() => this.showDetail(order)}
                      >
                        <span>
                          {order.order_items.map((item) => (
                            <TableRow> ⚫ {item.product.title} </TableRow>
                          ))}
                        </span>
                      </TableCell>

                      <TableCell
                        align="center"
                        style={{ width: "10%", cursor: "pointer" }}
                        onClick={() => this.showDetail(order)}
                      >
                        {order.created_at.split("T")[0]}
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ width: "15%", cursor: "pointer" }}
                        onClick={() => this.showDetail(order)}
                      >
                        {order.total} $
                      </TableCell>

                      <TableCell
                        style={{ width: "43%", cursor: "pointer" }}
                        align="left"
                        onClick={() => this.showDetail(order)}
                      >
                        <span style={{ marginLeft: "10px" }}>
                          <TableRow>
                            {order.first_name} {order.last_name}{" "}
                          </TableRow>
                          <TableRow>{order.phone_number} </TableRow>
                          <TableRow>
                            {order.address.province} / {order.address.city} /{" "}
                            {order.address.address}
                          </TableRow>{" "}
                        </span>
                      </TableCell>

                      <TableCell align="center" style={{ width: "10%" }}>
                        <Select
                          fullWidth
                          style={{ color: "red", height: "60px" }}
                          variant="outlined"
                          value={order.order_items[0].status}
                          name={order}
                          onChange={this.handleChangeCat.bind(this)}
                        >
                          <MenuItem fullWidth value={false} name={order}>
                            <ListItem style={{ color: "red" }}>
                              <ListItemIcon>
                                <CachedIcon style={{ color: "red" }} />
                              </ListItemIcon>
                              <ListItemText primary="pending" />
                            </ListItem>
                          </MenuItem>

                          <MenuItem value={true} name={order}>
                            {" "}
                            <ListItem style={{ color: "green" }}>
                              <ListItemIcon>
                                <CheckCircleOutlineIcon
                                  style={{ color: "green" }}
                                />{" "}
                              </ListItemIcon>
                              <ListItemText primary=" deliverd" />
                            </ListItem>
                          </MenuItem>
                        </Select>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Container>
          </Container>
        </div>
      </TableCell>
    );

    return (
      <div className={mainBgStyle} style={mainBgStyle}>
        {/* <div style={secondaryBgStyle}>
          <Button
            disabled
            align="center"
            variant="contained"
            style={{
              align: "center",
              marginTop: "10px",
              fontSize: "20px",
              backgroundColor: "#EEEDE7",
              color: bgColor
            }}
          >
            your orders
          </Button>
        </div> */}
        <LoadingOverlay
          active={this.state.loading}
          spinner
          text="please wait ..."
        >
          <TableRow
            style={{
              height: "100%",
              width: "100%",
              backgroundColor: "#EEEDE7",
            }}
          >
            <Hidden mdDown="true">
              <ProfileSliderLayout type={this.props.match.params.type} />
            </Hidden>
            {this.state.isLoading === false
              ? this.state.ordersData.length === 0
                ? noOrderLayout
                : ordersLayout
              : null}

            {dialog}
          </TableRow>{" "}
          <div
            style={{
              height: "200px",
              backgroundColor: "#EEEDE7",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          ></div>
        </LoadingOverlay>
      </div>
    );
  }
}

ProfileOrdersPage.propTypes = {
  auth: PropTypes.object.isRequired,
  getSalesRecords: PropTypes.func.isRequired,
  getOrders: PropTypes.func.isRequired,
  changeStatus: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, {
  getSalesRecords,
  getOrders,
  changeStatus,
})(ProfileOrdersPage);

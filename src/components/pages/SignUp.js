import React, { Component } from "react";
import PropType from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { signupUser } from "./../../actions/authActions";

export class SignUp extends Component {
  constructor() {
    super();
    this.state = {
      first_name: "",
      last_name: "",
      username: "",
      email: "",
      phone_number: "",
      password: "",
      password2: "",
      avatar: "",
      errors: {},
      is_producer:""
    };
  }

  componentDidMount() {
    const { user } = this.props.match.params      
    if (user == "producer"){
      this.setState({is_producer:true})
    }
    else{
      this.setState({is_producer:false})
    }
    
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/");
    }
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit = e => {
    e.preventDefault();

    const newUser = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      username: this.state.username,
      email: this.state.email,
      phone_number: this.state.phone_number,
      password: this.state.password,
      password2: this.state.password2,
      is_producer : this.state.is_producer
    };
    this.props.signupUser(newUser, this.props.history);
  };
  render() {
    return (
      <Container component="main" maxWidth="sm">
        <CssBaseline />
        <div>
          <Container align="center">
          <AccountCircleIcon
              align="center"
              style={{ width: "200px", height: "200px", align: "center" }}
            />
           
          </Container>
          <form noValidate onSubmit={this.onSubmit} style={{marginTop:"30px"}} >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="fname"
                  name="first_name"
                  variant="outlined"
                  required
                  fullWidth
                  id="first_name"
                  label="First Name"
                  autoFocus
                  value={this.state.name}
                  onChange={this.onChange}
                  error={Boolean(this.state.errors.first_name)}
                  type="text"
                />
                {this.props.errors.first_name ? (
                  <div style={{ color: "red" }}>
                    {this.props.errors.first_name}
                    <br />{" "}
                  </div>
                ) : null}
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="last_name"
                  label="Last Name"
                  name="last_name"
                  autoComplete="lname"
                  value={this.state.last_name}
                  onChange={this.onChange}
                  error={Boolean(this.state.errors.last_name)}
                  type="text"
                />
                {this.props.errors.last_name ? (
                  <div style={{ color: "red" }}>
                    {this.props.errors.last_name}
                    <br />{" "}
                  </div>
                ) : null}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="username"
                  label="Username"
                  name="username"
                  autoFocus
                  value={this.state.username}
                  onChange={this.onChange}
                  error={Boolean(this.state.errors.username)}
                  type="text"
                />
                {this.props.errors.username ? (
                  <div style={{ color: "red" }}>
                    {this.props.errors.username}
                    <br />{" "}
                  </div>
                ) : null}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  value={this.state.email}
                  onChange={this.onChange}
                  error={Boolean(this.state.errors.email)}
                  type="email"
                />
                {this.props.errors.email ? (
                  <div style={{ color: "red" }}>
                    {this.props.errors.email}
                    <br />{" "}
                  </div>
                ) : null}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="phone"
                  label="Phone"
                  name="phone_number"
                  autoComplete="phone"
                  value={this.state.phone_number}
                  onChange={this.onChange}
                  error={Boolean(this.state.errors.phone_number)}
                  type="text"
                />
                {this.props.errors.phone_number ? (
                  <div style={{ color: "red" }}>
                    {this.props.errors.phone_number}
                    <br />{" "}
                  </div>
                ) : null}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  value={this.state.password}
                  onChange={this.onChange}
                  error={Boolean(this.state.errors.password)}
                />
                {this.props.errors.password ? (
                  <div style={{ color: "red" }}>
                    {this.props.errors.password}
                    <br />{" "}
                  </div>
                ) : null}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  name="password2"
                  label="Confirm Password"
                  type="password"
                  id="password2"
                  autoComplete="current-password"
                  value={this.state.password2}
                  onChange={this.onChange}
                  error={Boolean(this.state.errors.password2)}
                  type="password"
                />
                {this.props.errors.password2 ? (
                  <div style={{ color: "red" }}>
                    {this.props.errors.password2}
                    <br />{" "}
                  </div>
                ) : null}
              </Grid>
            </Grid>
            <Button
              style={{ backgroundColor: "#011627", marginTop: 15, padding: 10 }}
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
            >
              Sign Up
            </Button>
            <Grid container>
              <Grid item>
                <Link href="login" variant="body2">
                  <span style={{ color: "#011627" }}>
                    <Typography
                      style={{
                        marginTop: 5,
                        fontSize: "10pt",
                        marginLeft: 20
                      }}
                    >
                      Already have an account? Login
                    </Typography>
                  </span>
                </Link>
              </Grid>
            </Grid>
          </form>
          <div>
            {this.props.errors.UserError ? (
              <div style={{ color: "red" }}>
                This username already exist
                <br />{" "}
              </div>
            ) : null}
          </div>
        </div>
      </Container>
    );
  }
}

SignUp.propTypes = {
  signupUser: PropType.func.isRequired,
  auth: PropType.object.isRequired,
  errors: PropType.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { signupUser })(withRouter(SignUp));

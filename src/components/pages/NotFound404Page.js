import React, { Component } from 'react'

export default class NotFound404Page extends Component {
    render() {
        return (
            <div  style={{
                backgroundImage:
                  "url(" + require("./../../statics/404.webp") + ")",
                height: "99%",
                width:"100%",
                margin: 0,
                backgroundPosition: "center",
                backgroundSize: "cover",

               
              }}>
               
            </div>
        )
    }
}

import { Component } from "react";
import React from "react";
import { Container, Typography, CardActionArea } from "@material-ui/core";
import GridList from "@material-ui/core/GridList";
import axios from "axios";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import CardView from "../layouts/CardView";
import CartLayout from "../layouts/CartLayout";
import {
  getCategory,
  getCategoryPage,
  getCategoryTotalPages,
} from "../../actions/productsActions";
import { bgcolor } from "@material-ui/system";
import LoadingOverlay from "react-loading-overlay";
import Pagination from "material-ui-flat-pagination";
const imageURL =
  "https://www.pythonanywhere.com/user/hampa2/files/home/hampa2/";
const listStyle3 = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
};
const bgColor = "#EEEDE7";
const listStyle2 = {
  background: bgColor,
  backgroundColor: bgColor,
  height: "60px",
};

class ProductsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      search: "",
      inCart: [],
      category: "",
      loading: true,
      isLoading: true,
      offset: 8,
      totolProducts: 16,
      pageSize: 8,
      page: 1,
    };
  }
  async handleClick(offset) {
    this.props.history.push({
      pathname: "/s",
      state: {
        offset: offset / 8 + 1,
        category: this.state.category,
        type: "products",
      },
    });
  }
  async componentDidMount() {
    const { category, page } = this.props.match.params;
    await this.setState({ category: category, offset: (page - 1) * 8 });
    const res = await this.props.getCategoryPage(category, page);
    const res2 = await this.props.getCategoryTotalPages(category);
    console.log(res.data);
    await this.setState({
      totolProducts: res2.data.product_number,
      data: res.data,
      loading: false,
    });
  }

  render() {
    return (
      <div align="center" style={listStyle3}>
        <LoadingOverlay
          active={this.state.loading}
          spinner
          text="please wait ..."
        >
          <div style={listStyle2}>
            <Button
              disabled
              align="center"
              variant="contained"
              style={{
                align: "center",
                marginTop: "10px",
                color: "#011627",
                fontSize: "20px",
                backgroundColor: bgColor,
              }}
            >
              {this.state.category}{" "}
            </Button>
          </div>
          <br></br>
          <Container align="center" style={listStyle3}>
            <GridList cols={4}>
              {this.state.data.map((product) => (
                <CardView
                  width={270}
                  height={350}
                  name={product.title}
                  price={product.price}
                  product={product}
                  src={imageURL + product.image}
                />
              ))}
            </GridList>
          </Container>
          <CartLayout />
          {this.state.data.length != 0 ? (
            <div
              style={{
                height:
                  window.innerHeight -
                    Math.ceil(this.state.data.length / 4) * 400 -
                    200 >
                  0
                    ? window.innerHeight -
                      Math.ceil(this.state.data.length / 4) * 400 -
                      200
                    : 0,
              }}
            ></div>
          ) : (
            <div
              style={{
                height:
                  window.innerHeight -
                    parseInt(this.state.data.length / 4) * 400 -
                    200 >
                  0
                    ? window.innerHeight -
                      parseInt(this.state.data.length / 4) * 400 -
                      200
                    : 0,
              }}
            ></div>
          )}
          <Pagination
            style={{
              justifyContent: "center",
              alignItems: "center",
              align: "center",
              marginTop: "20px",
            }}
            limit={this.state.pageSize}
            offset={this.state.offset}
            total={this.state.totolProducts}
            onClick={(e, offset) => this.handleClick(offset)}
          />
          <div style={{ height: "20px" }}></div>
        </LoadingOverlay>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  cart: state.cart,
  auth: state.auth,
});
ProductsPage.propTypes = {
  cart: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  getCategory: PropTypes.func.isRequired,
  getCategoryPage: PropTypes.func.isRequired,
  getCategoryTotalPages: PropTypes.func.isRequired,
};
export default connect(mapStateToProps, {
  getCategory,
  getCategoryPage,
  getCategoryTotalPages,
})(ProductsPage);

import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import { Component } from "react";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import StoreIcon from "@material-ui/icons/Store";
import { Link } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Hidden from "@material-ui/core/Hidden";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import ProfileSliderLayout from "../layouts/ProfileSliderLayout";
import { Container } from "semantic-ui-react";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import AddIcon from "@material-ui/icons/Add";
import { Button, Grid } from "@material-ui/core";
import {
  getAccountInfo,
  getProfileInfo,
  addNewAddress,
  deleteAddress,
} from "../../actions/authActions";
import Avatar from "@material-ui/core/Avatar";
import FaceIcon from "@material-ui/icons/Face";
import Proptypes from "prop-types";
import axios from "axios";
import DeleteForeverIcon from "@material-ui/icons/Minimize";
import LoadingOverlay from "react-loading-overlay";

const imageURL =
  "https://www.pythonanywhere.com/user/hampa2/files/home/hampa2/";

const listStyle = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
};
const textColor1 = "#727369";
const textColor2 = "#011627";

export class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addresses: [
        {
          province: "svsdvsd",
          city: "sdvsdvsd",
          address: "dfbdfbxfdb xfgbxfb",
        },
        {
          province: "svsdvsd",
          city: "sdvsdvsd",
          address: "dfbdfbxfdb xfgbxfb",
        },
      ],
      user: "",
      userProfile: "",
      isOpen: false,
      province: "",
      address: "",
      city: "",
      loading: true,
    };
    this.addNewAdddressClicked = this.addNewAdddressClicked.bind(this);
    this.handleAddNewAddress = this.handleAddNewAddress.bind(this);
  }
  async componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      this.props.history.push("/login");
    }
    const { type } = this.props.match.params;
    const res = await this.props.getAccountInfo();
    const res2 = await this.props.getProfileInfo();
    await this.setState({
      user: res.data,
      userProfile: res2.data,
      addresses: res2.data.addresses,
      loading: false,
    });
  }

  async onClickDeleteAddress(id) {
    await this.props.deleteAddress(id);
    const res2 = await this.props.getProfileInfo();
    await this.setState({
      userProfile: res2.data,
      addresses: res2.data.addresses,
    });
  }
  cancelAddressBtn() {
    this.setState({ isOpen: false });
  }
  handleAddNewAddress() {
    this.setState({ isOpen: true, province: "", city: "", address: "" });
  }

  addNewAdddressClicked = async (e) => {
    e.preventDefault();
    await this.props.addNewAddress(
      this.state.province,
      this.state.city,
      this.state.address
    );
    const res2 = await this.props.getProfileInfo();
    await this.setState({
      userProfile: res2.data,
      addresses: res2.data.addresses,
    });
    this.setState({ isOpen: false });
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  render() {
    const addressesLayout = (
      <Grid style={{ width: "1000px", border: "1px solid #EEEDE7" }}>
        <br></br> <br></br> <br></br>
        <TableCell width="20%" style={{ border: "none" }}>
          <div align="left">
            <LocationOnIcon />{" "}
            <span style={{ fontSize: "19px" }}> your address</span>
          </div>
        </TableCell>
        <TableRow>
          <TableCell width="70%" style={{ backgroundColor: "#F1F1F1" }}>
            {this.state.addresses.map((address) => (
              <div className="radio" style={{ marginLeft: "20px" }}>
                <label>
                  <p style={{ fontSize: "17px" }} />
                  <DeleteForeverIcon
                    onClick={() => this.onClickDeleteAddress(address.id)}
                    style={{
                      marginRight: "20px",
                      width: "25px",
                      height: "25px",
                      color: textColor2,
                      cursor: "pointer",
                    }}
                  />
                  <span
                    style={{
                      flexGrow: 1,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    {address.province} / {address.city} / {address.address}
                  </span>
                </label>
              </div>
            ))}
          </TableCell>
          <TableCell
            width="30%"
            align="left"
            style={{ backgroundColor: "#F1F1F1", marginLeft: "700px" }}
          >
            <Button
              align="right"
              onClick={this.handleAddNewAddress}
              variant="outlined"
              style={{
                marginLeft: "50px",
                marginRight: "100px",

                backgroundColor: "#f1f1f1",
              }}
            >
              <AddIcon />
              <span style={{ fontSize: "12px" }}>new address</span>
            </Button>
          </TableCell>
        </TableRow>
      </Grid>
    );
    const noAddressLayout = (
      <Grid style={{ width: "1000px", border: "1px solid #EEEDE7" }}>
        <br></br> <br></br> <br></br>
        <TableRow>
          <TableCell width="20%" style={{ backgroundColor: "#F1F1F1" }}>
            <div align="left">
              <LocationOnIcon />{" "}
              <span style={{ fontSize: "19px" }}> your address</span>
            </div>
          </TableCell>
          <TableCell width="50%" style={{ backgroundColor: "#F1F1F1" }}>
            <form style={{ marginLeft: "30px", backgroundColor: "#F1F1F1" }}>
              {this.state.addresses.map((address) => (
                <div className="radio">
                  <label>
                    <p type="radio" value={address.id} checked={true} />
                    {address.city}:{address.address}
                  </label>
                </div>
              ))}
            </form>
          </TableCell>
          <TableCell
            width="30%"
            align="left"
            style={{ backgroundColor: "#F1F1F1", marginLeft: "700px" }}
          >
            <Button
              align="right"
              onClick={this.handleAddNewAddress}
              variant="outlined"
              style={{
                marginLeft: "50px",
                marginRight: "100px",

                backgroundColor: "#f1f1f1",
              }}
            >
              <AddIcon />
              <span style={{ fontSize: "12px" }}>new address</span>
            </Button>
          </TableCell>
        </TableRow>
      </Grid>
    );
    return (
      <div className={listStyle} style={{ backgroundColor: "#EEEDE7" }}>
        <LoadingOverlay
          active={this.state.loading}
          spinner
          text="please wait ..."
        >
          <TableRow
            style={{
              height: "100%",
              width: "100%",
              backgroundColor: "#EEEDE7",
            }}
          >
            <ProfileSliderLayout type={this.props.match.params.type} />
            <TableCell
              style={{
                height: "100%",
                backgroundColor: "#EEEDE7",
                border: "none",
              }}
            >
              <div style={{ marginLeft: "200px", marginTop: "60px" }}>
                <div
                  style={{
                    display: "flex",
                    marginBottom: "30px",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  {!(this.state.userProfile.image === null) ? (
                    <Avatar
                      alt={this.state.user.username}
                      src={imageURL + this.state.userProfile.image}
                      style={{
                        width: "100px",
                        height: "100px",
                        align: "center",
                      }}
                    />
                  ) : (
                    <AccountCircleIcon
                      style={{
                        width: "100px",
                        height: "100px",
                        align: "center",
                      }}
                    />
                  )}
                </div>
                <TableRow
                  style={{ backgroundColor: "#f9f9f9", height: "110px" }}
                >
                  <TableCell
                    style={{ width: "500px", border: "1px solid #EEEDE7" }}
                  >
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "17px",
                        color: textColor1,
                      }}
                    >
                      name :
                    </Typography>
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "20px",
                        color: textColor2,
                      }}
                    >
                      {" "}
                      {this.state.user.first_name}
                    </Typography>
                  </TableCell>
                  <TableCell
                    style={{ width: "500px", border: "1px solid #EEEDE7" }}
                  >
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "17px",
                        color: textColor1,
                      }}
                    >
                      lastname :
                    </Typography>
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "20px",
                        color: textColor2,
                      }}
                    >
                      {" "}
                      {this.state.user.last_name}
                    </Typography>
                  </TableCell>
                </TableRow>
                <TableRow
                  style={{ backgroundColor: "#f9f9f9", height: "110px" }}
                >
                  <TableCell
                    style={{ width: "500px", border: "1px solid #EEEDE7" }}
                  >
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "17px",
                        color: textColor1,
                      }}
                    >
                      username :
                    </Typography>
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "20px",
                        color: textColor2,
                      }}
                    >
                      {" "}
                      {this.state.user.username}
                    </Typography>
                  </TableCell>
                  <TableCell
                    style={{ width: "500px", border: "1px solid #EEEDE7" }}
                  >
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "17px",
                        color: textColor1,
                      }}
                    >
                      phone number :
                    </Typography>
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "20px",
                        color: textColor2,
                      }}
                    >
                      {" "}
                      {this.state.user.phone_number}
                    </Typography>
                  </TableCell>
                </TableRow>
                <TableRow
                  style={{ backgroundColor: "#f9f9f9", height: "110px" }}
                >
                  <TableCell
                    style={{
                      width: "500px",
                      border: "1px solid #EEEDE7",
                      borderRight: "none",
                    }}
                  >
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "17px",
                        color: textColor1,
                      }}
                    >
                      email :
                    </Typography>
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "20px",
                        color: textColor2,
                      }}
                    >
                      {" "}
                      {this.state.user.email}
                    </Typography>
                  </TableCell>
                  <TableCell
                    style={{ width: "500px", border: "1px solid #EEEDE7" }}
                  >
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "17px",
                        color: textColor1,
                      }}
                    >
                      credit :
                    </Typography>
                    <Typography
                      style={{
                        marginLeft: "5px",
                        fontSize: "20px",
                        color: textColor2,
                      }}
                    >
                      {" "}
                      {this.state.userProfile.credit}
                    </Typography>
                  </TableCell>
                </TableRow>

                {this.state.addresses.length === 0
                  ? noAddressLayout
                  : addressesLayout}
              </div>
            </TableCell>
          </TableRow>

          <Dialog
            style={{ width: "100%", height: "100%" }}
            aria-labelledby="customized-dialog-title"
            open={this.state.isOpen}
          >
            <div
              style={{
                width: "100%",
                height: "100%",
                backgroundColor: "#ffffff",
              }}
            >
              <form
                noValidate
                onSubmit={this.addNewAdddressClicked}
                style={{ margin: "20px" }}
              >
                <TextField
                  margin="20px"
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="province"
                  label="province"
                  id="province"
                  autoFocus
                  onChange={this.onChange}
                />
                <TextField
                  margin="20px"
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="city"
                  label="city"
                  name="city"
                  autoFocus
                  onChange={this.onChange}
                />

                <TextField
                  margin="20px"
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="address"
                  label="address"
                  id="address"
                  autoFocus
                  onChange={this.onChange}
                />
                <Grid container spacing={2} style={{ width: "100%" }}>
                  <Grid item xs={12} sm={6}>
                    <Button
                      style={{ backgroundColor: "#011627", padding: 10 }}
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                    >
                      add
                    </Button>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Button
                      fullWidth
                      style={{ backgroundColor: "#011627", padding: 10 }}
                      variant="contained"
                      color="primary"
                      onClick={this.cancelAddressBtn.bind(this)}
                    >
                      cancel
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </div>
          </Dialog>

          <div
            style={{
              height: "100px",
              backgroundColor: "#EEEDE7",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          ></div>
        </LoadingOverlay>
      </div>
    );
  }
}

ProfilePage.propTypes = {
  auth: PropTypes.object.isRequired,
  getProfileInfo: Proptypes.func.isRequired,
  getAccountInfo: Proptypes.func.isRequired,
  addNewAddress: Proptypes.func.isRequired,
  deleteAddress: Proptypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, {
  getAccountInfo,
  getProfileInfo,
  addNewAddress,
  deleteAddress,
})(ProfilePage);

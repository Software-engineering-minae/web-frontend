import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import StarRatingComponent from "react-star-rating-component";
import StarIcon from "@material-ui/icons/Star";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import { Component } from "react";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import StoreIcon from "@material-ui/icons/Store";
import { Link } from "@material-ui/core";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Hidden from "@material-ui/core/Hidden";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import ProfileSliderLayout from "../layouts/ProfileSliderLayout";
import Dialog from "@material-ui/core/Dialog";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import AddIcon from "@material-ui/icons/Add";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import axios from "axios";
import {
  deleteProduct,
  getUserProducts,
  editProduct,
  addProduct,
  getCategories,
} from "../../actions/productsActions";
import { getAccountInfo } from "../../actions/authActions";
import LoadingOverlay from "react-loading-overlay";
import IconButton from "@material-ui/core/IconButton";

const imageURL =
  "https://www.pythonanywhere.com/user/hampa2/files/home/hampa2/";

const bgColor2 = "#ffffff";
const bgColor = "#011627";
const mainBgStyle = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
};

const secondaryBgStyle = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
  height: "60px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

export class ProfileStorePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      productsLen: 1,
      btnText: "edit",
      rating: 5,
      dialogIsOpen: false,
      editedProductId: "",
      editedProductImage: "",
      editedProductUrl: "",
      title: "",
      detail: "",
      price: "",
      count: "",
      image: "",
      dialogBtnText: "add product",
      imageWidth: "200px",
      imageHeight: "200px",
      titleError: { error: false, text: "" },
      detailError: { error: false, text: "" },
      priceError: { error: false, text: "" },
      countError: { error: false, text: "" },
      imageError: { error: false, text: "" },
      catError: { error: false, text: "" },
      category: [],
      dialogCat: "none",
      formData: [],
      loading: true,
    };
  }
  async deleteProduct(product) {
    const res = await this.props.deleteProduct(product.id);
    const rest2 = await this.props.getUserProducts();
    this.setState({ products: rest2.data, productsLen: rest2.data.length });
  }
  async addNewProduct() {
    await this.setState({
      dialogBtnText: "add product",
      dialogIsOpen: "true",
      title: "",
      detail: "",
      price: "",
      count: "",
      image: "",
      dialogCat: "",
    });
  }
  async onClickEditBtn(product) {
    var categoryId = 0;
    var tmp = 1;
    this.state.category.forEach((cat) => {
      cat.tag === product.tags[0] ? (categoryId = cat.id) : (tmp = cat.id);
    });

    await this.setState({
      dialogIsOpen: "true",
      dialogBtnText: "edit",
      title: product.title,
      detail: product.detail,
      price: product.price,
      image: imageURL + product.image,
      editedProductUrl: product.image,
      count: product.count,
      dialogCat: categoryId,
      editedProductId: product.id,
      editedProductImage: imageURL + product.image,
    });
  }
  async handleChangeCat(event) {
    await this.setState({ dialogCat: event.target.value });
    console.log(this.state.dialogCat);
  }
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  imageOnError() {
    this.setState({ image: "http://uupload.ir/files/uja_11111111.png" });
  }
  async onCancelDialog() {
    await this.setState({ dialogIsOpen: false });
  }
  async onSubmitDialog() {
    if (this.state.title == "") {
      await this.setState({
        titleError: { error: true, text: "can't be empty" },
      });
    } else {
      await this.setState({ titleError: { error: false, text: "" } });
    }

    if (this.state.detail == "") {
      await this.setState({
        detailError: { error: true, text: "can't be empty" },
      });
    } else {
      await this.setState({ detailError: { error: false, text: "" } });
    }
    if (this.state.dialogCat == "none" || this.state.dialogCat == "") {
      await this.setState({
        catError: { error: true, text: "choose a category" },
      });
    } else {
      await this.setState({ catError: { error: false, text: "" } });
    }

    if (this.state.count + "" == "") {
      await this.setState({
        countError: { error: true, text: "can't be empty" },
      });
    } else {
      if (
        (parseInt(this.state.count) + "").length !==
        (this.state.count + "").length
      ) {
        await this.setState({
          countError: { error: true, text: "should be a number" },
        });
      } else {
        await this.setState({ countError: { error: false, text: "" } });
      }
    }

    if (this.state.price + "" == "") {
      await this.setState({
        priceError: { error: true, text: "can't be empty" },
      });
    } else {
      if (
        (parseInt(this.state.price) + "").length !==
        (this.state.price + "").length
      ) {
        await this.setState({
          priceError: { error: true, text: "should be a number" },
        });
      } else {
        await this.setState({ priceError: { error: false, text: "" } });
      }
    }
    if (this.state.image == "http://uupload.ir/files/uja_11111111.png") {
      await this.setState({
        imageError: { error: true, text: "you should upload an image" },
      });
    } else {
      await this.setState({ imageError: { error: false, text: "" } });
    }

    if (
      this.state.titleError.error == false &&
      this.state.detailError.error == false &&
      this.state.priceError.error == false &&
      this.state.countError.error == false &&
      this.state.imageError.error == false &&
      this.state.catError.error == false
    ) {
      if (this.state.dialogBtnText == "edit") {
        if (this.state.image == this.state.editedProductImage) {
          var bodyFormData = new FormData();
        } else {
          var bodyFormData = this.state.formData;
        }

        bodyFormData.set("title", this.state.title);
        bodyFormData.set("detail", this.state.detail);
        bodyFormData.set("price", this.state.price);
        bodyFormData.set("count", this.state.count);
        bodyFormData.set("tags", this.state.dialogCat);
        bodyFormData.set("productID", this.state.editedProductId);

        const res = await this.props.editProduct(bodyFormData);
        const rest2 = await this.props.getUserProducts();
        await this.setState({
          dialogIsOpen: false,
          products: rest2.data,
          productsLen: rest2.data.length,
        });
      } else if (this.state.dialogBtnText == "add product") {
        var bodyFormData = this.state.formData;
        bodyFormData.set("title", this.state.title);
        bodyFormData.set("detail", this.state.detail);
        bodyFormData.set("price", this.state.price);
        bodyFormData.set("count", this.state.count);
        bodyFormData.set("tags", this.state.dialogCat);

        const res = await this.props.addProduct(bodyFormData);
        const rest2 = await this.props.getUserProducts();
        await this.setState({
          dialogIsOpen: false,
          products: rest2.data,
          productsLen: rest2.data.length,
        });
      }
    }
  }

  onImageChange = async (event) => {
    let image = event.target.files;

    if (image && image[0]) {
      let fd = new FormData();
      fd.append("image", image[0]);
      await this.setState({
        formData: fd,
        image: URL.createObjectURL(image[0]),
      });
    }
  };
  async componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      await this.props.history.push("/login");
    } else {
      const res = await this.props.getAccountInfo();
      if (res.data.is_producer == false) {
        await this.props.history.push("/404not_found");
      }
    }
    const res = await this.props.getCategories();
    const rest2 = await this.props.getUserProducts();
    this.setState({
      category: res.data,
      products: rest2.data,
      productsLen: rest2.data.length,
      loading: false,
    });
  }
  render() {
    const noProductLayout = (
      <TableCell
        style={{
          height: "100%",
          width: "99%",
          backgroundColor: "#EEEDE7",
          border: "none",
        }}
      >
        <div align="center" style={mainBgStyle}>
          <Container>
            <br></br>
            <TableRow style={{ width: "100%", backgroundColor: "#f4f3f1" }}>
              <TableCell style={{ width: "40%", border: "none" }}>
                <ErrorOutlineIcon
                  style={{
                    margin: "1px",
                    color: "#b7b7a4",
                    width: "300px",
                    height: "300px",
                  }}
                />
              </TableCell>
              <TableCell
                style={{
                  display: "flex",
                  width: "70%",
                  height: "260px",
                  border: "none",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <ul style={{ listStyle: "none" }}>
                  <li>
                    <Typography
                      style={{
                        color: "#a5a5a5",
                        fontSize: "20px",
                        fontWeight: "bold",
                      }}
                    >
                      {" "}
                      you dont have any product yet
                    </Typography>
                  </li>
                  <li style={{ marginTop: "20px" }}>
                    <Typography
                      style={{
                        color: "#a5a5a5",
                        fontSize: "20px",
                        fontWeight: "bold",
                      }}
                    >
                      please add your first one
                    </Typography>
                  </li>
                </ul>
              </TableCell>
            </TableRow>
          </Container>
        </div>
      </TableCell>
    );

    const productsLayout = (
      <TableCell
        style={{
          height: "100%",
          width: "99%",
          backgroundColor: "#EEEDE7",
          border: "none",
        }}
      >
        {this.state.products.map((product) => (
          <div align="center" style={mainBgStyle}>
            <Container>
              <br></br>
              <TableRow
                style={{
                  width: "100%",
                  backgroundColor: "#f4f3f1",
                  width: "950px",
                }}
              >
                <TableCell style={{ width: "40%" }}>
                  <img
                    style={{ margin: "1px" }}
                    src={imageURL + product.image}
                    width="300px"
                    height="300px"
                  />
                </TableCell>
                <TableCell style={{}}>
                  <div
                    style={{
                      backgroundColor: "#f9f9f9",
                      height: "280px",
                      width: "600px",
                      overflowY: "scroll",
                    }}
                  >
                    <ul style={{ listStyle: "none" }}>
                      <li style={{ marginTop: "20px", fontSize: "20px" }}>
                        <TableRow>
                          <TableCell style={{ border: "none" }}>
                            <p
                              style={{
                                fontSize: "22px",
                                fontStyle: "oblique",
                              }}
                            >
                              {" "}
                              {product.title}{" "}
                            </p>
                          </TableCell>
                          <TableCell style={{ border: "none" }}>
                            <div>
                              <TableRow>
                                <TableCell style={{ border: "none" }}>
                                  <p
                                    style={{
                                      color: "#727369",
                                      fontSize: "14px",
                                      marginBottom: "20px",
                                    }}
                                  >
                                    total rate :
                                  </p>
                                </TableCell>
                                <TableCell style={{ border: "none" }}>
                                  <StarRatingComponent
                                    name="showRate"
                                    editing={false}
                                    starCount={5}
                                    value={product.rate}
                                    renderStarIcon={() => (
                                      <StarIcon
                                        style={{
                                          height: "20px",
                                          width: "20px",
                                        }}
                                      />
                                    )}
                                  />
                                </TableCell>
                              </TableRow>
                            </div>
                          </TableCell>
                        </TableRow>
                      </li>
                      <li
                        style={{
                          marginLeft: "12px",
                          fontSize: "17px",
                          color: "#727369",
                        }}
                      >
                        <span>{product.detail}</span>
                      </li>
                      <li
                        style={{
                          marginLeft: "10px",

                          marginTop: "10px",
                          fontSize: "17px",
                          color: "#727369",
                        }}
                      >
                        {" "}
                        <span style={{ fontWeight: "bold" }}>price :</span>
                        <span>{product.price} $ </span>
                        <span
                          style={{ marginLeft: "60px", fontWeight: "bold" }}
                        >
                          count :
                        </span>
                        <span> {product.count}</span>
                      </li>
                    </ul>

                    <div style={{}}>
                      <span style={{ marginLeft: "50px" }}>
                        <Button
                          onClick={() => this.onClickEditBtn(product)}
                          style={{
                            color: bgColor2,
                            backgroundColor: bgColor,
                            fontSize: "16px",
                            width: "100px",
                            height: "40px",
                          }}
                        >
                          {this.state.btnText}
                        </Button>
                      </span>{" "}
                      <span style={{ marginLeft: "100px" }}>
                        <IconButton aria-label="delete">
                          <DeleteForeverIcon
                            onClick={() => this.deleteProduct(product)}
                            style={{
                              width: "30px",
                              height: "30px",
                              cursor: "pointer",
                            }}
                          />
                        </IconButton>
                      </span>
                    </div>
                  </div>
                </TableCell>
              </TableRow>
            </Container>
          </div>
        ))}
      </TableCell>
    );

    return (
      <div className={mainBgStyle} style={mainBgStyle}>
        <LoadingOverlay
          active={this.state.loading}
          spinner
          text="please wait ..."
        >
          <TableRow
            style={{
              height: "100%",
              width: "100%",
              backgroundColor: "#EEEDE7",
            }}
          >
            <Hidden mdDown="true">
              <ProfileSliderLayout type={this.props.match.params.type} />
            </Hidden>

            {this.state.productsLen === 0 ? noProductLayout : productsLayout}

            <Dialog
              style={{ width: "100%", height: "100%" }}
              aria-labelledby="customized-dialog-title"
              open={this.state.dialogIsOpen}
            >
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  backgroundColor: "#ffffff",
                }}
              >
                <form noValidate style={{ margin: "30px" }}>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="title"
                        label="title"
                        name="title"
                        autoFocus
                        value={this.state.title}
                        onChange={this.onChange}
                        error={Boolean(this.state.titleError.error)}
                        type="text"
                      />

                      {this.state.titleError.error ? (
                        <div style={{ color: "red" }}>
                          {this.state.titleError.text}
                          <br />{" "}
                        </div>
                      ) : null}
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="detail"
                        label="detail"
                        name="detail"
                        autoFocus
                        value={this.state.detail}
                        error={this.state.detailError.error}
                        onChange={this.onChange}
                      />
                      {this.state.detailError.error ? (
                        <div style={{ color: "red" }}>
                          {this.state.detailError.text}
                          <br />{" "}
                        </div>
                      ) : null}
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <TextField
                        name="price"
                        variant="outlined"
                        required
                        fullWidth
                        id="price"
                        label="price"
                        autoFocus
                        value={this.state.price}
                        error={this.state.priceError.error}
                        onChange={this.onChange}
                        type="text"
                      />
                      {this.state.priceError.error ? (
                        <div style={{ color: "red" }}>
                          {this.state.priceError.text}
                          <br />{" "}
                        </div>
                      ) : null}
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <TextField
                        name="count"
                        variant="outlined"
                        required
                        fullWidth
                        autoFocus
                        id="available count"
                        label="available count"
                        value={this.state.count}
                        onChange={this.onChange}
                        error={this.state.countError.error}
                        type="text"
                      />
                      {this.state.countError.error ? (
                        <div style={{ color: "red" }}>
                          {this.state.countError.text}
                          <br />{" "}
                        </div>
                      ) : null}
                    </Grid>{" "}
                    <Grid item xs={12} sm={4}>
                      <FormControl
                        variant="outlined"
                        fullWidth
                        error={this.state.catError.error}
                        autoFocus
                      >
                        <Select
                          native
                          value={this.state.dialogCat}
                          onChange={this.handleChangeCat.bind(this)}
                          inputProps={{
                            name: "category",
                            id: "outlined-category-native-simple",
                          }}
                        >
                          {" "}
                          <option value={"none"}></option>
                          {this.state.category.map((cat) => (
                            <option value={cat.id}>{cat.tag}</option>
                          ))}
                        </Select>
                      </FormControl>
                      {this.state.catError.error ? (
                        <div style={{ color: "red" }}>
                          {this.state.catError.text}
                          <br />{" "}
                        </div>
                      ) : null}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <img
                        onError={this.imageOnError.bind(this)}
                        src={this.state.image}
                        style={{
                          width: this.state.imageWidth,
                          height: this.state.imageHeight,
                          backgroundColor: "#ffffff",
                        }}
                      ></img>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <Button
                        startIcon={<CloudUploadIcon />}
                        variant="contained"
                        component="label"
                      >
                        upload
                        <input
                          onChange={this.onImageChange}
                          type="file"
                          style={{ display: "none" }}
                        />
                      </Button>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      {this.state.imageError.error ? (
                        <div style={{ color: "red" }}>
                          {this.state.imageError.text}
                          <br />{" "}
                        </div>
                      ) : null}
                    </Grid>{" "}
                    <Grid item xs={12} sm={6}></Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <Button
                        onClick={this.onSubmitDialog.bind(this)}
                        style={{
                          color: bgColor2,
                          backgroundColor: bgColor,
                          marginTop: 15,
                          padding: 10,
                          width: "140px",
                        }}
                        variant="contained"
                        color="primary"
                      >
                        {this.state.dialogBtnText}
                      </Button>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <Button
                        onClick={this.onCancelDialog.bind(this)}
                        style={{
                          color: bgColor2,
                          backgroundColor: bgColor,
                          marginTop: 15,
                          padding: 10,
                          width: "140px",
                        }}
                        variant="contained"
                        color="primary"
                      >
                        cancel
                      </Button>
                    </Grid>
                  </Grid>
                </form>
              </div>
            </Dialog>
          </TableRow>
          <div
            style={{
              backgroundColor: "#EEEDE7",

              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Button
              style={{
                marginBottom: "30px",
                color: bgColor2,
                backgroundColor: bgColor,
                marginTop: "30px",
                height: "50px",
              }}
              startIcon={<AddIcon style={{ width: "40px", height: "40px" }} />}
              variant="contained"
              onClick={this.addNewProduct.bind(this)}
              component="label"
            >
              <Typography> add new product</Typography>
            </Button>
          </div>
          <div
            style={{
              height: "200px",
              backgroundColor: "#EEEDE7",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          ></div>
        </LoadingOverlay>
      </div>
    );
  }
}

ProfileStorePage.propTypes = {
  auth: PropTypes.object.isRequired,
  deleteProduct: PropTypes.func.isRequired,
  addProduct: PropTypes.func.isRequired,
  getUserProducts: PropTypes.func.isRequired,
  editProduct: PropTypes.func.isRequired,
  getCategories: PropTypes.func.isRequired,
  getAccountInfo: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, {
  deleteProduct,
  addProduct,
  editProduct,
  getUserProducts,
  getCategories,
  getAccountInfo,
})(ProfileStorePage);

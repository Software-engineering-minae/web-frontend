import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import withAutoplay from "react-awesome-slider/dist/autoplay";
import "react-awesome-slider/dist/styles.css";
import AwesomeSlider from "react-awesome-slider";
import "./HomePage.css";
import { Container } from "@material-ui/core";
import Footer from "../layouts/Footer";
import MyScrollMenu from "../layouts/ScrollMenu/MyScrollMenu";
import CartLayout from "../layouts/CartLayout";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";
import { connect } from "react-redux";
import axios from "axios";
import {
  getCategories,
  getAllProducts,
  getNews,
  getPopularProducts,
} from "../../actions/productsActions";
import PropTypes from "prop-types";
import LoadingOverlay from "react-loading-overlay";

const imageURL =
  "https://www.pythonanywhere.com/user/hampa2/files/home/hampa2/";
const bgColor = "#011627";
const bgColor2 = "#ffffff";

const listStyle2 = {
  background: bgColor,
  backgroundColor: bgColor,
  height: "60px",
};
const AutoplaySlider = withAutoplay(AwesomeSlider);

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: [],
      tmp: {},
      scrollMenuData: [],
      urls: { left: "", right: [] },
      loading: true,
    };
  }

  async componentDidMount() {
    const categories = await this.props.getCategories();

    await this.setState({ category: categories.data });
    const products = await this.props.getPopularProducts();
    const news = await this.props.getNews();
    await this.setState({
      scrollMenuData: products.data,
      urls: news.data,
      loading: false,
    });
  }
  onClickSlider() {
    this.props.history.push({ pathname: "/signup/producer" });
  }
  render() {
    const slider = (
      <AutoplaySlider
        style={{ zIndex: "1" }}
        play={true}
        cancelOnInteraction={false} // should stop playing on user interaction
        interval={6000}
      >
        {this.state.urls.right.map((url) => (
          <div data-src={url} />
        ))}
      </AutoplaySlider>
    );
    return (
      <div
        align="center"
        style={{ height: "100%", width: "100%", paddingBottom: 100 }}
      >
        <LoadingOverlay active={this.state.loading} spinner text="loading ...">
          <Container align="center">
            <TableRow
              style={{
                display: "flex",
                listStyle: "none",
              }}
            >
              <TableCell
                style={{
                  width: "30%",
                  marginTop: "10px",
                  marginRight: "10px",
                  marginBottom: "10px",
                }}
              >
                <img
                  src={this.state.urls.left}
                  alt="not loaded"
                  style={{ width: "100%", height: "100%" }}
                />
              </TableCell>
              <TableCell
                style={{ margin: "10px", width: "70%", border: "none" }}
              >
                <div align="center">{slider}</div>
              </TableCell>
            </TableRow>

            <div className="Homepage" style={{ marginTop: "40px" }}></div>
          </Container>

          <div
            width="100%"
            style={{ backgroundColor: "#f4f3f1", marginTop: "30px" }}
          >
            <Container>
              <Table aria-label="orders">
                <TableHead>
                  <TableRow>
                    {this.state.category.map((cat) => (
                      <TableCell align="center" style={{ width: "25%" }}>
                        <h1 style={{ fontSize: "15px" }}> {cat.tag} </h1>
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow height="40%">
                    {this.state.category.map((cat) => (
                      <TableCell
                        style={{ border: "none", width: "25%", height: 290 }}
                      >
                        <img
                          alt="not loaded"
                          src={cat.URL}
                          width="100%"
                          height="100%"
                          onClick={() => {
                            this.props.history.push({
                              pathname: "/products/" + cat.tag + "/1",
                            });
                          }}
                          style={{ cursor: "pointer" }}
                        />
                      </TableCell>
                    ))}
                  </TableRow>
                </TableBody>
              </Table>
            </Container>
            <br></br>
          </div>

          <div style={{ backgroundColor: "#EEEDE7" }}>
            <Container>
              <div style={listStyle2}>
                <TableRow align="left" width="100%">
                  <TableCell align="left" style={{ border: "none" }}>
                    <Button
                      align="left"
                      style={{
                        backgroundColor: bgColor,
                        color: bgColor2,
                      }}
                      variant="contained"
                      disabled
                    >
                      Bestseller
                    </Button>
                  </TableCell>
                  <TableCell>
                    <Button
                      align="left"
                      style={{
                        backgroundColor: bgColor,

                        color: bgColor2,
                      }}
                      variant="contained"
                      disabled
                    ></Button>
                  </TableCell>
                  <TableCell style={{ border: "none" }} width="25%"></TableCell>
                  <TableCell width="25%" style={{ border: "none" }}></TableCell>
                  <TableCell width="25%" style={{ border: "none" }}></TableCell>
                </TableRow>
              </div>
            </Container>
            <br></br>
            <Container style={{ height: "300px" }}>
              <MyScrollMenu
                data={this.state.scrollMenuData}
                data2="0"
                margin="70px"
                bgColor="#EEEDE7"
              />
            </Container>
          </div>
          {/* <div style={{ backgroundColor: "#f4f3f1" }}>
          <Container>
            <TableRow align="left" width={Window.innerWidth}>
              <TableCell
                align="left"
                style={{ border: "none" }}
                width={window.innerWidth}
              >
                <Button
                  align="left"
                  style={{
                    backgroundColor: "#f4f3f1",
                    marginLeft: 30,
                    color: "#727369"
                  }}
                  variant="contained"
                  disabled
                >
                  Best seller
                </Button>
                <Button
                  align="left"
                  style={{
                    backgroundColor: "#f4f3f1",
                    marginLeft: 30,
                    color: "#727369"
                  }}
                  variant="contained"
                  disabled
                >
                  More +
                </Button>
              </TableCell>
              <TableCell style={{ border: "none" }} width="25%"></TableCell>
              <TableCell width="25%" style={{ border: "none" }}></TableCell>
              <TableCell width="25%" style={{ border: "none" }}></TableCell>
            </TableRow>

            <MyScrollMenu />
          </Container>
                </div>*/}

          <Footer />
          <CartLayout />
        </LoadingOverlay>
      </div>
    );
  }
}

HomePage.propTypes = {
  getCategories: PropTypes.func.isRequired,
  getAllProducts: PropTypes.func.isRequired,
  getNews: PropTypes.func.isRequired,
  getPopularProducts: PropTypes.func.isRequired,
};

export default connect(null, {
  getCategories,
  getAllProducts,
  getNews,
  getPopularProducts,
})(HomePage);

import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import "react-awesome-slider/dist/styles.css";
import "./HomePage.css";
import { Container } from "@material-ui/core";
import MyScrollMenu from "../layouts/ScrollMenu/MyScrollMenu";
import CartLayout from "../layouts/CartLayout";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { connect } from "react-redux";
import axios from "axios";
import StarRatingComponent from "react-star-rating-component";
import StarIcon from "@material-ui/icons/Star";
import { deleteOrder, addOrder, retrieveCart } from "../../actions/cartActions";
import {
  likeComment,
  getComments,
  addComment,
  getRate,
  checkRate,
  addRate,
  getProductDetail,
  getAllProducts,
} from "../../actions/productsActions";
import { Grid } from "@material-ui/core";
import PropTypes from "prop-types";
import ThumbUpAltIcon from "@material-ui/icons/ThumbUpAlt";
import ThumbDownIcon from "@material-ui/icons/ThumbDown";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Avatar from "@material-ui/core/Avatar";
import FaceIcon from "@material-ui/icons/Face";
import LoadingOverlay from "react-loading-overlay";

const imageURL =
  "https://www.pythonanywhere.com/user/hampa2/files/home/hampa2/";
const listStyle2 = {
  background: "#f4f3f1",
  backgroundColor: "#f4f3f1",
  height: "60px",
  marginTop: "30px",
};

const listStyle = {
  background: "#EEEDE7",
  backgroundColor: "#EEEDE7",
};

class ProductPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: { hashtags: [] },
      category: [],
      tmp: {},
      scrollMenuData: [],
      rating: 1,
      canRate: true,
      bgColor1: "#f4f3f1",
      bgColor2: "#ffffff",
      bgColor3: "#EEEDE7",
      bgColor4: "#d2d3d9",
      btnText: "",
      inCart: false,
      text: "",
      comments: [],
      isLoggedIn: "false",
      commentBtn: "",
      commentText: "",
      cursor1: "normal",
      userProfile: "",
      isMouseOnRating: false,
      availableCountError: false,
      loading: true,
      hashtags: [],
    };
  }
  async likeComment(comment) {
    await this.props.likeComment(comment.id, "like");
    const comments = await this.props.getComments(this.state.product.id);
    await this.setState({ comments: comments.data });
  }
  async dislikeComment(comment) {
    await this.props.likeComment(comment.id, "dislike");
    const comments = await this.props.getComments(this.state.product.id);
    await this.setState({ comments: comments.data });
  }

  async updateCommentInput(event) {
    await this.setState({ commentText: event.target.value });
  }
  async onClickCommentBtn() {
    if (this.state.isLoggedIn == true) {
      await this.props.addComment(
        this.state.product.id,
        this.state.commentText
      );
      const comments = await this.props.getComments(this.state.product.id);
      await this.setState({ comments: comments.data });
    } else {
      this.props.history.push("/login");
    }
  }
  searchHashtag(value) {
    this.props.history.push({
      pathname: "/s",
      state: { detail: value, type: "search", page: 1 },
    });
  }

  async onClickBtn() {
    if (this.state.product.count > 0) {
      if (this.state.inCart == true) {
        this.props.deleteOrder(this.state.product.id);
      } else {
        this.props.addOrder(this.state.product);
      }
      await this.setState({ inCart: !this.state.inCart });

      if (this.state.inCart) {
        await this.setState({ btnText: "remove from cart" });
      } else {
        await this.setState({ btnText: "add to cart" });
      }
    } else {
      if (this.state.inCart == true) {
        this.props.deleteOrder(this.state.product.id);
        await this.setState({
          inCart: !this.state.inCart,
          btnText: "remove from cart",
        });
      } else {
        await this.setState({ availableCountError: true });
      }
    }
  }
  async componentWillReceiveProps(nextProps) {
    if (!nextProps.auth.isAuthenticated) {
      await this.setState({
        isLoggedIn: false,
        commentBtn: "login first",
        cursor1: "normal",
      });
    } else {
      await this.setState({
        isLoggedIn: true,
        commentBtn: "comment",
        cursor1: "pointer",
      });
    }
  }

  async componentDidMount() {
    const { id, title } = this.props.match.params;
    const producrRes = await this.props.getProductDetail(id);
    await this.setState({
      product: producrRes.data,
      hashtags: producrRes.data.hashtags.split(","),
    });
    if (!this.props.auth.isAuthenticated) {
      await this.setState({
        isLoggedIn: false,
        commentBtn: "login first",
        cursor1: "normal",
      });
    } else {
      await this.setState({
        isLoggedIn: true,
        commentBtn: "comment",
        cursor1: "pointer",
      });
    }
    if (this.state.isLoggedIn == true) {
      var result = false;
      var result2 = false;
      const res = await this.props.retrieveCart();
      res.data.cart_items.forEach((order) => {
        this.state.product.id === order.product.id
          ? (result = true)
          : (result2 = false);
      });
      await this.setState({ inCart: result });
    } else {
      var result = false;
      var result2 = false;
      this.props.cart.cart.forEach((order) => {
        this.state.product.id === order.product.id
          ? (result = true)
          : (result2 = false);
      });
      await this.setState({ inCart: result });
    }

    if (this.state.inCart) {
      await this.setState({ btnText: "remove from cart" });
    } else {
      await this.setState({ btnText: "add to cart" });
    }
    await this.setState({ rating: 3 });
    await this.setState({ canRate: true });
    const res2 = await this.props.getAllProducts();

    await this.setState({ scrollMenuData: res2.data, loading: false });
    const res3 = await this.props.getComments(this.state.product.id);

    await this.setState({ comments: res3.data });

    if (this.state.isLoggedIn == false) {
      await this.setState({ canRate: false });
    } else {
      const res5 = await this.props.checkRate(this.state.product.id);
      if (res5.data.error == true) {
        await this.setState({ canRate: false });
      } else {
        this.setState({ canRate: true });
      }
    }

    const res6 = await this.props.getRate(this.state.product.id);
    this.setState({ rating: res6.data.rate });
  }

  async onStarClick(nextValue, prevValue, name) {
    const res1 = await this.props.addRate(this.state.product.id, nextValue);
    const res = await this.props.getRate(this.state.product.id);
    this.setState({ rating: res.data.rate, canRate: !this.state.canRate });
  }
  async mouseEnterRating() {
    this.setState({ isMouseOnRating: true });
  }
  async mouseLeaveRating() {
    this.setState({ isMouseOnRating: false });
  }
  render() {
    const showRateLayout = (
      <div>
        <TableRow>
          <TableCell style={{ border: "none" }}>
            <p
              style={{
                color: "#727369",
                fontSize: "14px",
                marginBottom: "20px",
              }}
            >
              total rate :
            </p>
          </TableCell>
          <TableCell style={{ border: "none" }}>
            <StarRatingComponent
              name="showRate"
              editing={false}
              starCount={5}
              value={this.state.rating}
              renderStarIcon={() => (
                <StarIcon
                  onMouseEnter={this.mouseEnterRating.bind(this)}
                  onMouseLeave={this.mouseLeaveRating.bind(this)}
                  style={{ height: "20px", width: "20px" }}
                />
              )}
              onStarClick={this.onStarClick.bind(this)}
            />
          </TableCell>
          {this.state.isMouseOnRating && !this.state.isLoggedIn ? (
            <TableCell style={{ border: "none", color: "red" }}>
              login first
            </TableCell>
          ) : null}
          {this.state.isMouseOnRating && this.state.isLoggedIn ? (
            <TableCell style={{ border: "none", color: "red" }}>
              already rated
            </TableCell>
          ) : null}
        </TableRow>
      </div>
    );
    const rateLayout = (
      <div>
        <TableRow>
          <TableCell style={{ border: "none" }}>
            <p
              style={{
                color: "#727369",
                fontSize: "14px",
                marginBottom: "20px",
              }}
            >
              rate now :
            </p>
          </TableCell>
          <TableCell style={{ border: "none" }}>
            <StarRatingComponent
              name="rate"
              starCount={5}
              value={this.state.rating}
              renderStarIcon={() => (
                <StarIcon style={{ height: "20px", width: "20px" }} />
              )}
              onStarClick={this.onStarClick.bind(this)}
            />
          </TableCell>
        </TableRow>
      </div>
    );

    return (
      <div className={listStyle} style={{ backgroundColor: "#EEEDE7" }}>
        <LoadingOverlay active={this.state.loading} spinner text="loading ...">
          <Container
            className={listStyle}
            style={{ backgroundColor: "#EEEDE7" }}
          >
            <br></br>
            <TableRow style={{ width: "100%", width: "950px" }}>
              <Grid container spacing={2}>
                <Grid item>
                  <TableCell style={{ width: "40%" }}>
                    <img
                      style={{ margin: "1px" }}
                      src={imageURL + this.state.product.image}
                      width="400px"
                      height="400px"
                    />
                  </TableCell>
                </Grid>{" "}
                <Grid item>
                  <TableCell style={{}}>
                    <div
                      style={{
                        backgroundColor: "#f7f7f7",
                        height: "360px",
                        width: "650px",
                      }}
                    >
                      <ul style={{ listStyle: "none" }}>
                        <li style={{ marginTop: "30px", fontSize: "22px" }}>
                          <TableRow>
                            <TableCell style={{ border: "none" }}>
                              <p
                                style={{
                                  fontSize: "25px",
                                  fontStyle: "oblique",
                                }}
                              >
                                {" "}
                                {this.state.product.title}{" "}
                              </p>
                            </TableCell>
                            <TableCell style={{ border: "none" }}>
                              {this.state.canRate ? rateLayout : showRateLayout}
                            </TableCell>
                          </TableRow>
                        </li>
                        <li
                          style={{
                            marginLeft: "12px",
                            fontSize: "19px",
                            color: "#727369",
                          }}
                        >
                          {this.state.product.detail}
                        </li>
                        <li
                          style={{
                            marginLeft: "12px",

                            marginTop: "10px",
                            fontSize: "17px",
                            color: "#727369",
                          }}
                        >
                          {" "}
                          <span style={{ fontWeight: "bold" }}>price :</span>
                          <span>{this.state.product.price} $ </span>
                          <span
                            style={{ marginLeft: "60px", fontWeight: "bold" }}
                          >
                            available count :
                          </span>
                          <span> {this.state.product.count}</span>
                        </li>
                        <li style={{ marginTop: "15px" }}>
                          {this.state.hashtags.length >= 1 ? (
                            <span
                              onClick={() =>
                                this.searchHashtag(this.state.hashtags[0])
                              }
                              style={{
                                cursor: "pointer",
                                backgroundColor: this.state.bgColor4,
                                fontSize: "15px",
                                margin: "7px",
                              }}
                            >
                              <span style={{ margin: "5px", color: "#727369" }}>
                                {this.state.hashtags[0]}
                              </span>
                            </span>
                          ) : null}

                          {this.state.hashtags.length >= 2 ? (
                            <span
                              onClick={() =>
                                this.searchHashtag(this.state.hashtags[1])
                              }
                              style={{
                                cursor: "pointer",
                                backgroundColor: this.state.bgColor4,
                                fontSize: "15px",
                                margin: "7px",
                              }}
                            >
                              <span style={{ margin: "5px", color: "#727369" }}>
                                {this.state.hashtags[1]}
                              </span>
                            </span>
                          ) : null}
                          {this.state.hashtags.length >= 3 ? (
                            <span
                              onClick={() =>
                                this.searchHashtag(this.state.hashtags[2])
                              }
                              style={{
                                cursor: "pointer",
                                backgroundColor: this.state.bgColor4,
                                fontSize: "15px",
                                margin: "7px",
                              }}
                            >
                              <span style={{ margin: "5px", color: "#727369" }}>
                                {this.state.hashtags[2]}
                              </span>
                            </span>
                          ) : null}
                          {this.state.hashtags.length >= 4 ? (
                            <span
                              onClick={() =>
                                this.searchHashtag(this.state.hashtags[3])
                              }
                              style={{
                                cursor: "pointer",
                                backgroundColor: this.state.bgColor4,
                                fontSize: "15px",
                                margin: "7px",
                              }}
                            >
                              <span style={{ margin: "5px", color: "#727369" }}>
                                {this.state.hashtags[3]}
                              </span>
                            </span>
                          ) : null}
                        </li>
                        <li style={{ marginTop: "8px" }}>
                          {this.state.hashtags.length >= 5 ? (
                            <span
                              onClick={() =>
                                this.searchHashtag(this.state.hashtags[4])
                              }
                              style={{
                                cursor: "pointer",
                                backgroundColor: this.state.bgColor4,
                                fontSize: "15px",
                                margin: "7px",
                              }}
                            >
                              <span style={{ margin: "5px", color: "#727369" }}>
                                {this.state.hashtags[4]}
                              </span>
                            </span>
                          ) : null}
                          {this.state.hashtags.length >= 6 ? (
                            <span
                              onClick={() =>
                                this.searchHashtag(this.state.hashtags[5])
                              }
                              style={{
                                cursor: "pointer",
                                backgroundColor: this.state.bgColor4,
                                fontSize: "15px",
                                margin: "7px",
                              }}
                            >
                              <span style={{ margin: "5px", color: "#727369" }}>
                                {this.state.hashtags[5]}
                              </span>
                            </span>
                          ) : null}
                          {this.state.hashtags.length >= 7 ? (
                            <span
                              onClick={() =>
                                this.searchHashtag(this.state.hashtags[6])
                              }
                              style={{
                                cursor: "pointer",
                                backgroundColor: this.state.bgColor4,
                                fontSize: "15px",
                                margin: "7px",
                              }}
                            >
                              <span style={{ margin: "5px", color: "#727369" }}>
                                {this.state.hashtags[6]}
                              </span>
                            </span>
                          ) : null}
                          {this.state.hashtags.length >= 8 ? (
                            <span
                              onClick={() =>
                                this.searchHashtag(this.state.hashtags[7])
                              }
                              style={{
                                cursor: "pointer",
                                backgroundColor: this.state.bgColor4,
                                fontSize: "15px",
                                margin: "7px",
                              }}
                            >
                              <span style={{ margin: "5px", color: "#727369" }}>
                                {this.state.hashtags[7]}
                              </span>
                            </span>
                          ) : null}
                        </li>
                      </ul>
                      <br></br>

                      <div
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <Button
                          onClick={this.onClickBtn.bind(this)}
                          style={{
                            color: "#ffffff",
                            backgroundColor: "#011627",
                            fontSize: "18px",
                            width: "220px",
                            height: "50px",
                          }}
                        >
                          {this.state.btnText}
                        </Button>
                        {this.state.availableCountError ? (
                          <p
                            style={{
                              border: "none",
                              color: "red",
                              marginLeft: "5px",
                            }}
                          >
                            not available
                          </p>
                        ) : null}
                      </div>
                    </div>
                  </TableCell>
                </Grid>
              </Grid>
            </TableRow>
          </Container>

          <div style={{ backgroundColor: this.state.bgColor1 }}>
            <Container>
              <div style={listStyle2}>
                <TableRow align="left" width="100%">
                  <TableCell align="left" style={{ border: "none" }}>
                    <Button
                      align="left"
                      style={{
                        backgroundColor: this.state.bgColor1,
                        color: "#333333",
                      }}
                      variant="contained"
                      disabled
                    >
                      similar
                    </Button>
                  </TableCell>
                  <TableCell style={{ border: "none" }}>
                    <Button
                      align="left"
                      style={{
                        backgroundColor: this.state.bgColor1,

                        color: "#333333",
                      }}
                      variant="contained"
                      disabled
                    ></Button>
                  </TableCell>
                  <TableCell style={{ border: "none" }} width="25%"></TableCell>
                  <TableCell width="25%" style={{ border: "none" }}></TableCell>
                  <TableCell width="25%" style={{ border: "none" }}></TableCell>
                </TableRow>
              </div>
            </Container>
            <br></br>
            <Container>
              <MyScrollMenu
                data={this.state.scrollMenuData}
                data2={this.props.match.params.id}
                margin="70px"
              />
            </Container>
          </div>
          <div
            style={{ height: "400px", backgroundColor: this.state.bgColor2 }}
          >
            <br></br>
            <Container>
              <div>
                <TableRow align="left" width="100%">
                  <TableCell
                    align="left"
                    style={{ border: "none", width: "10%" }}
                  >
                    <Button
                      align="left"
                      style={{
                        backgroundColor: this.state.bgColor2,
                        color: "#d7385e",
                      }}
                      variant="contained"
                      disabled
                    >
                      comments
                    </Button>
                  </TableCell>
                  <TableCell
                    width="18%"
                    style={{ border: "none", width: "90%" }}
                  >
                    <Button
                      align="left"
                      style={{
                        backgroundColor: this.state.bgColor2,

                        color: "#333333",
                      }}
                      variant="contained"
                      disabled
                    ></Button>
                  </TableCell>
                </TableRow>
              </div>
            </Container>
            <div
              style={{ width: "100%", backgroundColor: this.state.bgColor2 }}
            >
              <Container>
                {this.state.comments.map((comment) => (
                  <div style={{ backgroundColor: this.state.bgColor3 }}>
                    <TableRow>
                      <TableCell style={{ width: "10%", border: "none" }}>
                        {!(comment.profile.image === "") ? (
                          <Avatar
                            alt={comment.user.username}
                            src={imageURL + comment.profile.image}
                            style={{ width: "80px", height: "80px" }}
                          />
                        ) : (
                          <AccountCircleIcon
                            style={{ width: "80px", height: "80px" }}
                          />
                        )}
                      </TableCell>
                      <TableCell style={{ width: "15%", border: "none" }}>
                        <ul style={{ listStyle: "none" }}>
                          <li style={{ fontSize: "16px" }}>
                            {comment.user.username}
                          </li>
                          <li style={{ fontSize: "12px", color: "#727369" }}>
                            {comment.date}
                          </li>
                        </ul>
                      </TableCell>
                      <TableCell
                        style={{ width: "8%", border: "none", color: "green" }}
                      >
                        <ThumbUpAltIcon
                          onClick={() => this.likeComment(comment)}
                          style={{
                            width: "25px",
                            height: "25px",
                            color: "green",
                            cursor: this.state.cursor1,
                          }}
                        />{" "}
                        {comment.like}
                      </TableCell>
                      <TableCell
                        style={{ width: "68%", border: "none", color: "red" }}
                      >
                        <ThumbDownIcon
                          onClick={() => this.dislikeComment(comment)}
                          style={{
                            width: "25px",
                            height: "25px",
                            color: "red",
                            cursor: this.state.cursor1,
                          }}
                        />{" "}
                        {comment.dislike}
                      </TableCell>
                    </TableRow>
                    <div>
                      <TableRow>
                        <TableCell style={{ width: "10%" }}></TableCell>
                        <TableCell style={{ width: "30%" }}>
                          <textarea
                            rows={4}
                            value={comment.text}
                            onChange={this.handleChange}
                            readOnly
                            style={{
                              width: "700px",
                              fontSize: "18px",
                              borderRadius: "1%",
                              backgroundColor: this.state.bgColor1,
                            }}
                          />
                        </TableCell>
                        <TableCell style={{ width: "60%" }}></TableCell>
                      </TableRow>
                      <div
                        style={{
                          width: "100%",
                          height: "10px",
                          backgroundColor: this.state.bgColor2,
                        }}
                      ></div>
                    </div>
                  </div>
                ))}

                <form>
                  <div
                    style={{
                      backgroundColor: this.state.bgColor3,
                      width: "100%",
                    }}
                  >
                    <TableRow align="left" width="100%">
                      <TableCell
                        align="left"
                        style={{ border: "none", width: "10%" }}
                      ></TableCell>
                      <TableCell
                        align="left"
                        style={{ border: "none", width: "30%" }}
                      >
                        <textarea
                          placeholder="your comment"
                          value={this.state.value}
                          onChange={this.updateCommentInput.bind(this)}
                          rows={6}
                          style={{
                            width: "700px",
                            fontSize: "18px",
                            backgroundColor: this.state.bgColor1,
                          }}
                        />
                      </TableCell>
                      <TableCell style={{ border: "none", width: "60%" }}>
                        <Button
                          onClick={this.onClickCommentBtn.bind(this)}
                          style={{
                            marginLeft: "80px",
                            backgroundColor: "#011627",
                            color: "#ffffff",
                            fontSize: "14px",
                          }}
                        >
                          {this.state.commentBtn}
                        </Button>
                      </TableCell>
                    </TableRow>
                  </div>
                </form>
                <div
                  style={{
                    backgroundColor: this.state.bgColor2,
                    width: "100%",
                    height: "70px",
                  }}
                ></div>
              </Container>
            </div>
          </div>

          <CartLayout />
        </LoadingOverlay>
      </div>
    );
  }
}
ProductPage.propTypes = {
  cart: PropTypes.object.isRequired,
  deleteOrder: PropTypes.func.isRequired,
  addOrder: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  getComments: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  getProductDetail: PropTypes.func.isRequired,
  retrieveCart: PropTypes.func.isRequired,
  getAllProducts: PropTypes.func.isRequired,
  checkRate: PropTypes.func.isRequired,
  addRate: PropTypes.func.isRequired,
  getRate: PropTypes.func.isRequired,

  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  cart: state.cart,
  auth: state.auth,
});

export default connect(mapStateToProps, {
  deleteOrder,
  addOrder,
  likeComment,
  retrieveCart,
  getAllProducts,
  getComments,
  addComment,
  getProductDetail,
  checkRate,
  addRate,
  getRate,
})(ProductPage);
